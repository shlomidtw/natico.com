<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Extrafee
 */


namespace Amasty\Extrafee\Model;

class Tax
{
    /**
     * @var \Magento\Tax\Model\Calculation
     */
    private $calculation;

    /**
     * @var \Amasty\Extrafee\Helper\Data
     */
    private $helper;

    public function __construct(
        \Magento\Tax\Model\Calculation $calculation,
        \Amasty\Extrafee\Helper\Data $helper
    ) {
        $this->calculation = $calculation;
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     *
     * @return \Magento\Framework\DataObject|null
     */
    private function getRateRequest(\Magento\Quote\Model\Quote $quote)
    {
        $taxClass = $this->helper->getScopeValue('tax/tax_class');

        if ($taxClass) {
            return $this->calculation->getRateRequest(
                $quote->getShippingAddress(),
                $quote->getBillingAddress(),
                $quote->getCustomerTaxClassId(),
                $quote->getStore(),
                $quote->getCustomerId()
            )->setProductClassId($taxClass);
        }

        return null;
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     *
     * @return float
     */
    public function getTaxRate(\Magento\Quote\Model\Quote $quote)
    {
        $rateRequest = $this->getRateRequest($quote);
        $rate = 0;

        if ($rateRequest) {
            $rate = $this->calculation->getRate($rateRequest);
        }

        return $rate;
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     *
     * @param array $appliedTax
     * @param int $taxAmount
     * @param int $baseTaxAmount
     *
     * @return array
     */
    public function getTaxBreakdown(\Magento\Quote\Model\Quote $quote, $appliedTax, $taxAmount = 0, $baseTaxAmount = 0)
    {
        $rateRequest = $this->getRateRequest($quote);

        if ($rateRequest) {
            $feeAppliedTax = current($this->calculation->getAppliedRates($rateRequest));

            if (isset($feeAppliedTax['percent'], $feeAppliedTax['id'])) {
                if (isset($appliedTax[$feeAppliedTax['id']])) {
                    $appliedTax[$feeAppliedTax['id']]['amount'] += $taxAmount;
                    $appliedTax[$feeAppliedTax['id']]['base_amount'] += $baseTaxAmount;
                } else {
                    $appliedTax[$feeAppliedTax['id']] = [
                        'amount' => $taxAmount,
                        'base_amount' => $baseTaxAmount,
                        'percent' => $feeAppliedTax['percent'],
                        'id' => $feeAppliedTax['id'],
                        'rates' => current($feeAppliedTax),
                        'item_id' => null,
                        'item_type' => null,
                        'associated_item_id' => null,
                        'process' => 0
                    ];
                }
            }
        }

        return $appliedTax;
    }
}
