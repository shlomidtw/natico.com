<?php
namespace Jewel\ProductFeed\Block\Index;

class Index extends \Magento\Framework\View\Element\Template
{

    protected $_productRepository;
    
    protected $_helperData;
    
    
    public function __construct(\Magento\Catalog\Block\Product\Context $context, 
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository, 
        \Jewel\ProductFeed\Helper\Data $helperData,
        array $data = [])
    {
        $this->_productRepository = $productRepository;
        $this->_helperData = $helperData;

        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function loadMyProduct($skuc)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        
        $data = $this->_helperData->importCsvFileFromFile();
        
        for($i=1; $i<count($data); $i++) {
            echo "<pre>";
//             print_r($data[$i]); // $data[$i] is an array with your csv columns as values.
            $produdtId = $this->_helperData->getProductIdBySku($data[$i][0]);
            
            if($produdtId){
                
echo $query = <<<SQL
INSERT INTO `itoris_dynamicproductoptions_options` (`product_id`, `store_id`, `configuration`, `form_style`, `appearance`, `css_adjustments`, `extra_js`, `absolute_pricing`, `absolute_sku`, `absolute_weight`) VALUES
({$produdtId}, 0, '[null,{"order":1,"cols":1,"rows":1,"removable":1,"title":"","fields":[null,{"order":1,"removable":true,"option_id":20,"id":20,"is_require":"1","hide_on_focus":1,"section_order":1,"type":"drop_down","internal_id":1,"visibility_action":"hidden","visibility":"visible","customer_group":"",
"title":"Imprint","items":[null,{"order":1,"price_type":"fixed","is_selected":0,"is_disabled":0,"row":{"sizcache":86,"sizset":0},"visibility_action":"hidden","visibility":"visible","customer_group":"0,1","sort_order":1,
"title":"Blank","price":{$data[$i][1]},"option_type_id":95,"option_id":20,"image_src":null,"tier_price":"[{\\"qty\\":49,\\"price\\":\\"{$data[$i][1]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":99,\\"price\\":\\"{$data[$i][2]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":249,\\"price\\":\\"{$data[$i][3]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":499,\\"price\\":\\"{$data[$i][4]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":500,\\"price\\":\\"{$data[$i][5]}\\",\\"price_type\\":\\"fixed\\"}]","internal_id":1},{"order":2,"price_type":"fixed","is_selected":0,"is_disabled":0,"row":{"sizcache":90,"sizset":0},"visibility_action":"hidden","visibility":"visible","customer_group":"0,1","sort_order":2,
"title":"1 COLOR","price":{$data[$i][6]},"option_type_id":96,"option_id":20,"image_src":null,"tier_price":"[{\\"qty\\":49,\\"price\\":\\"{$data[$i][6]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":99,\\"price\\":\\"{$data[$i][7]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":249,\\"price\\":\\"{$data[$i][8]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":499,\\"price\\":\\"{$data[$i][9]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":500,\\"price\\":\\"{$data[$i][10]}\\",\\"price_type\\":\\"fixed\\"}]","internal_id":2},{"order":3,"price_type":"fixed","is_selected":0,"is_disabled":0,"row":{"sizcache":94,"sizset":0},"visibility_action":"hidden","visibility":"visible","customer_group":"0,1","sort_order":3,
"title":"FULL COLOR","price":{$data[$i][11]},"option_type_id":97,"option_id":20,"image_src":null,"tier_price":"[{\\"qty\\":49,\\"price\\":\\"{$data[$i][11]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":99,\\"price\\":\\"{$data[$i][12]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":249,\\"price\\":\\"{$data[$i][13]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":499,\\"price\\":\\"{$data[$i][14]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":500,\\"price\\":\\"{$data[$i][15]}\\",\\"price_type\\":\\"fixed\\"}]","internal_id":3},{"order":4,"price_type":"fixed","is_selected":0,"is_disabled":0,"row":{"sizcache":98,"sizset":0},"visibility_action":"hidden","visibility":"visible","customer_group":"0,1","sort_order":4,
"title":"ENGRAVING",","tier_price":"[{\\"qty\\":49,\\"price\\":\\"{$data[$i][16]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":99,\\"price\\":\\"{$data[$i][17]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":249,\\"price\\":\\"{$data[$i][18]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":499,\\"price\\":\\"{$data[$i][19]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":500,\\"price\\":\\"{$data[$i][20]}\\",\\"price_type\\":\\"fixed\\"}]","price":{$data[$i][16]},"internal_id":7,"option_type_id":98,"option_id":20,"image_src":null},{"order":5,"price_type":"fixed","is_selected":0,"is_disabled":0,"row":{"sizcache":109,"sizset":0},"visibility_action":"hidden","visibility":"visible","customer_group":"0,1","sort_order":5,
"title":"FULL COLOR2","tier_price":"[{\\"qty\\":49,\\"price\\":\\"{$data[$i][21]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":99,\\"price\\":\\"{$data[$i][22]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":249,\\"price\\":\\"{$data[$i][23]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":499,\\"price\\":\\"{$data[$i][24]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":500,\\"price\\":\\"{$data[$i][25]}\\",\\"price_type\\":\\"fixed\\"}]","price":{$data[$i][21]},"internal_id":8,"option_type_id":99,"option_id":20,"image_src":null},{"order":6,"price_type":"fixed","is_selected":0,"is_disabled":0,"row":{"sizcache":117,"sizset":0},"visibility_action":"hidden","visibility":"visible","customer_group":"0,1","sort_order":6,
"title":"DEBOSS","tier_price":"[{\\"qty\\":49,\\"price\\":\\"{$data[$i][26]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":99,\\"price\\":\\"{$data[$i][27]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":249,\\"price\\":\\"{$data[$i][28]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":499,\\"price\\":\\"{$data[$i][29]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":500,\\"price\\":\\"{$data[$i][30]}\\",\\"price_type\\":\\"fixed\\"}]","price":{$data[$i][26]},"internal_id":9,"option_type_id":100,"option_id":20,"image_src":null},{"order":7,"price_type":"fixed","is_selected":0,"is_disabled":0,"row":{"sizcache":121,"sizset":0},"visibility_action":"hidden","visibility":"visible","customer_group":"2","sort_order":7,
"title":"Blank","price":{$data[$i][31]},"option_type_id":101,"option_id":20,"image_src":null,"tier_price":"[{\\"qty\\":49,\\"price\\":\\"{$data[$i][31]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":99,\\"price\\":\\"{$data[$i][32]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":249,\\"price\\":\\"{$data[$i][33]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":499,\\"price\\":\\"{$data[$i][34]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":500,\\"price\\":\\"{$data[$i][35]}\\",\\"price_type\\":\\"fixed\\"}]","internal_id":4},{"order":8,"price_type":"fixed","is_selected":0,"is_disabled":0,"row":{"sizcache":125,"sizset":0},"visibility_action":"hidden","visibility":"visible","customer_group":"2","sort_order":8,
"title":"1 COLOR","price":{$data[$i][36]},"option_type_id":102,"option_id":20,"image_src":null,"tier_price":"[{\\"qty\\":49,\\"price\\":\\"{$data[$i][36]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":99,\\"price\\":\\"{$data[$i][37]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":249,\\"price\\":\\"{$data[$i][38]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":499,\\"price\\":\\"{$data[$i][39]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":500,\\"price\\":\\"{$data[$i][40]}\\",\\"price_type\\":\\"fixed\\"}]","internal_id":5},{"order":9,"price_type":"fixed","is_selected":0,"is_disabled":0,"row":{"sizcache":129,"sizset":0},"visibility_action":"hidden","visibility":"visible","customer_group":"2","sort_order":9,
"title":"FULL COLOR","price":{$data[$i][41]},"option_type_id":103,"option_id":20,"image_src":null,"tier_price":"[{\\"qty\\":49,\\"price\\":\\"{$data[$i][41]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":99,\\"price\\":\\"{$data[$i][42]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":249,\\"price\\":\\"{$data[$i][43]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":499,\\"price\\":\\"{$data[$i][44]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":500,\\"price\\":\\"{$data[$i][45]}\\",\\"price_type\\":\\"fixed\\"}]","internal_id":6},{"order":10,"price_type":"fixed","is_selected":0,"is_disabled":0,"row":{"sizcache":133,"sizset":0},"visibility_action":"hidden","visibility":"visible","customer_group":"2","sort_order":10,
"title":"ENGRAVING","price":{$data[$i][46]},"internal_id":10,"tier_price":"[{\\"qty\\":49,\\"price\\":\\"{$data[$i][46]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":99,\\"price\\":\\"{$data[$i][47]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":249,\\"price\\":\\"{$data[$i][48]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":499,\\"price\\":\\"{$data[$i][49]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":500,\\"price\\":\\"{$data[$i][50]}\\",\\"price_type\\":\\"fixed\\"}]","option_type_id":104,"option_id":20,"image_src":null},{"order":11,"price_type":"fixed","is_selected":0,"is_disabled":0,"row":{"sizcache":137,"sizset":0},"visibility_action":"hidden","visibility":"visible","customer_group":"2","sort_order":11,
"title":"FULL COLOR2","price":{$data[$i][51]},"internal_id":11,"tier_price":"[{\\"qty\\":49,\\"price\\":\\"{$data[$i][51]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":99,\\"price\\":\\"{$data[$i][52]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":249,\\"price\\":\\"{$data[$i][53]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":499,\\"price\\":\\"{$data[$i][54]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":500,\\"price\\":\\"{$data[$i][55]}\\",\\"price_type\\":\\"fixed\\"}]","option_type_id":105,"option_id":20,"image_src":null},{"order":12,"price_type":"fixed","is_selected":0,"is_disabled":0,"row":{"sizcache":141,"sizset":0},"visibility_action":"hidden","visibility":"visible","customer_group":"2","sort_order":12,
"title":"DEBOSS","price":{$data[$i][56]},"internal_id":12,"tier_price":"[{\\"qty\\":49,\\"price\\":\\"{$data[$i][56]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":99,\\"price\\":\\"{$data[$i][57]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":249,\\"price\\":\\"{$data[$i][58]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":499,\\"price\\":\\"{$data[$i][59]}\\",\\"price_type\\":\\"fixed\\"},{\\"qty\\":500,\\"price\\":\\"{$data[$i][60]}\\",\\"price_type\\":\\"fixed\\"}]","option_type_id":106,"option_id":20,"image_src":null}],"default_select_title":"-- Please Select --","sort_order":1,"itoris_option_id":0,"img_src":null}]}]', 'list_div', 'on_product_view', '', '', 0, 0, 0);
SQL;
            }
            $connection->query($query);
            echo "</pre>";
            
            echo PHP_EOL;
        }
 
        
        return $this->_productRepository->get($skuc);
    }
    
    

}