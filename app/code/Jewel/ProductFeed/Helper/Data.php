<?php
namespace Jewel\ProductFeed\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{

    /**
     * CSV Processor
     *
     * @var \Magento\Framework\File\Csv
     */
    protected $__csvProcessor;
    
    protected $_dir;
    
    protected $_product;

    public function __construct(\Magento\Framework\File\Csv $_csvProcessor, \Magento\Framework\Filesystem\DirectoryList $directory, \Magento\Catalog\Model\Product $product)
    {
        $this->_csvProcessor = $_csvProcessor;
        $this->_dir = $directory;
        $this->_product = $product;
    }

    public function importFromCsvFile($file)
    {
        if (! isset($file['tmp_name'])){
            throw new \Magento\Framework\Exception\LocalizedException(__('Invalid file upload attempt.'));
        }
        
        $importProductRawData = $this->_csvProcessor->getData($file['tmp_name']);
        
        foreach ($importProductRawData as $rowIndex => $dataRow) {
            \Zend_Debug::dump($dataRow);
        }
        die();
    }
    
    public function importCsvFileFromFile(){
        // This is the directory where you put your CSV file.
        $directory = $this->_dir->getPath('var');
        
        // This is your CSV file.
        $file = $directory . '/natico_tier_price.csv';
        
        if (file_exists($file)) {
            return $this->_csvProcessor->getData($file);
            
            $data = $this->_csvProcessor->getData($file);
            // This skips the first line of your csv file, since it will probably be a heading. Set $i = 0 to not skip the first line.
            for($i=1; $i<count($data); $i++) {

//                     print_r($data[$i]); // $data[$i] is an array with your csv columns as values.
                    foreach ($data[$i] as $key => $value){
                        echo "<pre>";
//                         print_r($value);
                        echo "</pre>";
                    }
              
            }
        }else{
            throw new \Magento\Framework\Exception\LocalizedException(__($file.' Not found.'));
        }
    }
    
    
    
    public function getProductIdBySku($sku){
        $skus = ["60-2622","60-800","60-2700","80-4112","60-890STM","30-1960","60-3885","10-W450","60-CR-240","60-CR-230","10-45888W","10-517CL","60-3002","60-2021","10-DF187","60-42-NBL-WH","10-1159","60-PF-05","60-3360-BK","20-490-BK","60-2019","60-30-BK","60-PF-71","60-PF-60","60-BC-530","60-252-BK","60-1024","60-603SET","10-1160","60-AT-32","60-30-NBL","60-83-BK","30-1622RB","60-330-BK","10-662","10-101BK","60-G057","60-7115-BK","60-WS153","60-988","60-330-WH","60-PF-83","60-IM360-BK","20-490-BL","60-1510","60-4656-BR","60-PF-79BR","60-PF-10","60-I360-RD","60-130-SET","10-45888B","60-62-GN-WH","60-7268","10-809","60-BBQ-5","60-WS6082","60-3001","60-253-BK","60-7114-BK","30-1096-SET","60-PF-23","60-PF-57","60-G521B","60-ICB902-BL","10-1008-WD","60-7186-WH","60-ICB904-BL","10-1326MP","60-948-BK","10-5388M","60-83-NBL","60-70077","60-PF-34","60-BP-67BK","60-1014","10-1365","60-WS1888","60-1508","60-260FF-BK","60-BBQ-4","60-PF-39","60-PF-88","60-7378","60-30-GN","60-110-SL","60-62-BL-WH","60-1581","10-P792","60-PF-43BR","60-WS4376","10-AC124","60-I360-BK","40-151B","60-BBQ-3","30-258S","60-154-SET","60-600","60-PF-08","60-IPRO-360-BK","60-4632","60-122-BK-WH","60-260FF-CY","60-253-BL","60-1233-BK","60-112-LM-BK","60-83-RD-WH","40-151","30-470","60-PF-87","60-CL11B","60-115-BL","60-PF-65","60-8256-BK","30-1023-BK","60-BP-52RD","60-30-TL-WH","60-2504-BK","60-1446","10-U1104","60-7105-BK","10-908L","60-1672-WH","60-8256-RD","60-7101-BK","60-BL223","60-8873","60-108-SL-BK","60-909","60-83-GN-WH","60-8256-PK","60-PF-29BR","60-PF-30","60-PF-64GY","60-DB-11BK","60-G056","10-1370","60-1227-SL","60-152CS","60-WS154","60-BL123","60-PF-42","60-3723-NBL","60-2099","60-115-BK","60-1501","60-ICB902-PR","60-PF-63","60-PF-55BR","60-PF-70","60-PF-82","60-7129-SET","60-IM4-360-BK","60-1046","60-7372","60-7655","60-2504-BL","60-GT-10","60-2860","60-0145","60-MB-14BL","10-1020","60-ICB902-RD","60-PF-44","30-400","60-8129","60-BP-65GY","60-WS1450","60-PF-62BK","60-DB-11BL","60-DB-15BK","60-1015","60-260FF-SL","60-ET-25","60-8256-GN","60-PF-11BR","60-PF-52","60-PF-54","60-I360-DPK","60-BP-08BL","60-2826","60-BBQ-11","60-PF-41","60-170-DB","60-CL11S","60-DB-01BL","60-6886","60-112-TL-BK","60-159-SET","60-8248-OR","60-320S","60-1293-BK","30-4668","60-PF-66","60-1339-SET","60-ICB904-CL","60-DB-02GY","60-MB-18BK","60-50-BK","60-252","60-8125","60-MB-14BK","60-108-SL-BL","60-50-NBL","60-58-BK","60-1028","60-ICB904-RD","60-ICB902-CL","10-C105","60-PF-68","60-M607","60-WS3021","60-8130","60-PF-35BR","10-1223W","60-AT-28","60-IA360-NBL","60-170","60-171","60-7125","60-BP-64BK","60-DB-02BL","60-112-OR-BK","60-50-GY","60-AT-02","60-GT-5","60-8248-PK","60-8248-RD","60-260FF-IV","60-BP-08BK","60-46-BK","60-BBQ-605","60-8248-GN","60-2601-GN","60-PF-59","10-WT263","60-I360-OR","60-IA360-BK","60-4637","60-7336-BK","60-7336-OR","60-BP-57BK","60-TT-07BK","60-48-LED","60-250-BR","10-212","10-107","60-1512-SET","60-163-SET","60-6021-RD","60-607-SET","60-7700-BL","60-3004","60-102-BL","60-IPRO-360-NBL","60-PF-56","10-5388B","60-IPRO-360-GN","60-IPRO-360-PR","60-6154","60-BP-52BK","60-DB-11RD","60-160-SL","60-122-RD-WH","60-46-BK-TL","60-58-NBL","60-110-BK","60-7384-BK","60-153-SET","60-GT-3","60-251-YW","60-8248-BK","60-PF-50","60-PF-31","60-PF-46","60-PF-67","60-IA360-RD","60-6089","60-6094-BK","60-7336-BL","60-MB-21BK","60-1257","60-6475-BL","60-42-RD-WH","60-250-BL","60-1246","30-84207","60-7700-RD","60-8245-RD","10-WT1350","60-3005","60-253-SET","60-6020","10-3166","30-1021-SET","60-PF-72","60-I360-LBL","60-I360-PR","60-IA2-360-GN","60-IA2-360-NBL","60-IA360-LBL","60-IM4-360-LBL","60-IPRO-360-DPK","60-2572-BK","60-4659-BR","60-6120-BK","60-BP-29GY","60-LN-16BK","60-6475-CO","60-4710-ER","60-157-SET","60-7700-BK","60-8245-BK","60-ZB06","60-DB-18BL","60-PF-03","60-ZB07","60-1057","60-1506","30-258L","60-1472-SL","60-IM360-LPK","60-IPRO-360-WH","60-BP-52BL","60-CL06S","60-MB-24BK","60-42-BK-WH","60-46-BK-BL","60-46-BK-RD","60-4720-RF","60-58-GN","60-1166S","60-IPRO-360-LBL","60-MB-15BK","10-517GY","30-418","60-8120","60-PF-06","60-PF-58","60-PF-81","60-2602-SL","60-I360-LPK","60-IA2-360-LBL","60-IM360-RD","10-875","60-606-8GB","60-3422","60-3432","60-3507","60-BP-60BL","60-CL06B","60-MB-18RD","60-TT-07BL","60-165-BK","60-7704","60-CR-250","60-8248-BL","60-8256-BL","60-PF-73","60-62-RD-WH","60-DB-01BK","60-DB-01RD","60-MB-15BL","60-M2259","60-I360-WH","60-I360-YW","60-IA2-360-BR","60-IA2-360-PR","60-IA360-GN","60-ICB904-GN","30-245MP","60-IM4-360-BR","60-3368-BK","60-I650-PLUS-BK","60-I650-PLUS-NB","60-170-ORG","60-2572-BR","60-BP-29BL","60-CL15B","60-DB-15RD","60-LN-16BL","60-ZB11","60-2337","60-250FS-BK","60-7381-BL","60-4646","60-831","30-364","60-255-BL","10-3964L","10-166","60-5600-GN","60-62-BK-WH","60-G072","10-WT208","60-8205","60-1001","10-P604","60-PF-15","60-PF-40","60-158","60-I360-BR","60-IA2-360-RD","60-1288","10-1223B","60-2144","60-3698","30-159MP","60-3430-BL","60-3620-BK","60-5283","60-7128","60-CL21S","60-CL26B","60-MB-24BL","60-TT-07RD","60-ZB18","60-ZB19","60-108-SL-TL","60-161","60-250FS-WH","60-5757","60-ET-24","60-1503-SET","60-1511-SET","60-8245-GN","60-ICB902-GN","60-WS1451","60-255-BK","60-3677","60-8245-BL","60-6021-BL","10-82690","10-875S","60-151-SET","60-4107","60-5600-YW","60-7172-BK","60-IA360-CH","60-LN-13BK","60-LN-13RD","60-2930ST","10-1078","10-1113G","10-150TIN","60-I360-NBL","60-125S","60-8132","60-I360-GN","60-2602-BL","60-IPRO-360-RD","60-IA360-BR","60-IM180-BK","60-IM360-LBL","60-IM360-YW","60-IPRO-360-OR","60-IM360-BR","60-3723-GY","60-9028","60-9655","60-BP-57BL","60-BP-60BK","60-CL15S","60-CL19S","60-LN-16RD","60-MB-18BL","60-MB-21GN","60-MB-24RD","60-46-BK-GN","60-4730-GL","60-7381-BK","60-7384-BL","60-9481","60-IM360-DPK","60-910-BL","60-910-RD","10-A151","60-948-SL","30-2010","60-1221-BK","60-1515-SET","60-162-SET","60-CL05B","60-CL08B","60-CL19B","60-DB-18BU","60-ICB904-LM","60-ICB904-PR","60-LN-20GN","60-LN-20RD","60-PF-09","60-PF-13","60-PF-36","10-1008G","10-1008S","10-1113S","10-1307","60-IM4-360-PR","10-1523G","10-1523S","10-1525","10-1606","60-IM360-GN","30-ST10F","60-PF-14","60-2600-BK","60-2600-BL","60-2600-GN","60-2600-WH","60-2600-YW","60-IA2-360-BK","60-IM360-NBL","60-IM4-360-DPK","60-IM360-OR","60-I650-PLUS-CY","60-I650-PLUS-GD","60-I650-PLUS-RD","60-I650-PLUS-SL","60-1701","60-3430-BK","60-3620-NBL","60-3655","60-4522","60-4656-BK","60-4659-BK","60-7357-BK","60-7357-BL","60-9026","60-9430-BK","60-9430-BL","60-BP-57RD","60-CL10B","60-DB-15BL","60-TT-170","60-TT-17BK","60-TT-17BL","60-TT-17RD","60-ZB26","60-IM360-PR","60-IM4-360-NBL","60-M2260","60-156-SET","60-1582","60-2679","60-BBQ-705","60-3000","60-IM4-360-OR","60-IM4-360-RD","60-IM360-WH","60-CR-280","60-IM4-360-WH","60-IM4-360-GN"];
        
        $productId = $this->_product->getIdBySku($sku);
        if($productId){
            return $productId;
        }else{
            return false;
        }
    }
}
