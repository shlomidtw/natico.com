<?php
namespace MageArray\ExtraFee\Api\Data;

/**
 * Interface GrandTotalDetailsInterface
 * @api
 */
interface GrandTotalDetailsInterface
{
    /**
     * Get tax amount value
     *
     * @return float|string
     */
    public function getAmount();

    /**
     * @param string|float $amount
     * @return $this
     */
    public function setAmount($amount);
}
