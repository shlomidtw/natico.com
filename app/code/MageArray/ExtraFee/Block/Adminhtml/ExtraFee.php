<?php
namespace MageArray\ExtraFee\Block\Adminhtml;

/**
 * Class ExtraFee
 * @package MageArray\ExtraFee\Block\Adminhtml
 */
class ExtraFee extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_extraFee';
        $this->_blockGroup = 'MageArray_ExtraFee';
        $this->_headerText = __('Manage Extra Fee');
        $this->_addButtonLabel = __('Add New Extra Fee');
        parent::_construct();
    }
}
