<?php
namespace MageArray\ExtraFee\Block\Adminhtml\ExtraFee;

/**
 * Class Edit
 * @package MageArray\ExtraFee\Block\Adminhtml\ExtraFee
 */
class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'MageArray_ExtraFee';
        $this->_controller = 'adminhtml_extraFee';
        parent::_construct();
        $this->buttonList->update('save', 'label', __('Save ExtraFee'));
        $this->buttonList->update('delete', 'label', __('Delete'));
        $this->buttonList->add(
            'save_and_continue',
            [
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => [
                            'event' => 'saveAndContinueEdit',
                            'target' => '#edit_form'
                        ],
                    ],
                ],
            ],
            10
        );
    }

    /**
     * @return mixed
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl(
            '*/*/save',
            ['_current' => true, 'back' => 'edit', 'tab' => '{{tab_id}}']
        );
    }
}
