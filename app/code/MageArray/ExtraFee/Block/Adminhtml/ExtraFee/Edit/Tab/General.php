<?php
namespace MageArray\ExtraFee\Block\Adminhtml\ExtraFee\Edit\Tab;

use Magento\Store\Model\System\Store;

/**
 * Class General
 * @package MageArray\ExtraFee\Block\Adminhtml\ExtraFee\Edit\Tab
 */
class General extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * General constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \MageArray\ExtraFee\Model\Config\Source\Status $status
     * @param \MageArray\ExtraFee\Model\Config\Source\FeeType $feeType
     * @param \MageArray\ExtraFee\Model\Config\Source\FeeOption $feeOption
     * @param \MageArray\ExtraFee\Model\Config\Source\Categorylist $categorylist
     * @param \MageArray\ExtraFee\Model\Config\Source\Paymentmethod $paymentMethod
     * @param Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \MageArray\ExtraFee\Helper\Data $helper,
        \MageArray\ExtraFee\Model\Config\Source\Status $status,
        \MageArray\ExtraFee\Model\Config\Source\FeeType $feeType,
        \MageArray\ExtraFee\Model\Config\Source\FeeOption $feeOption,
        \MageArray\ExtraFee\Model\Config\Source\Categorylist $categorylist,
        \MageArray\ExtraFee\Model\Config\Source\Paymentmethod $paymentMethod,
        \MageArray\ExtraFee\Model\Config\Source\BooleanValue $booleanValue,
		\Magento\Config\Model\Config\Structure\Element\Dependency\FieldFactory $fieldFactory,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->_status = $status;
        $this->helper = $helper;
        $this->_feeType = $feeType;
        $this->_feeOption = $feeOption;
        $this->_categorylist = $categorylist;
        $this->_paymentMethod = $paymentMethod;
        $this->booleanValue = $booleanValue;
        $this->_systemStore = $systemStore;
		$this->_fieldFactory = $fieldFactory;
		$this->_storeManager = $context->getStoreManager();
    }

    /**
     * @return mixed
     */
    public function getExtraFee()
    {
        return $this->_coreRegistry->registry('extrafee');
    }
	
	public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }

    /**
     * @return mixed
     */
    protected function _prepareForm()
    {
        $model = $this->getExtraFee();
        $isElementDisabled = false;
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('page_');
        $fieldSet = $form->addFieldset('base_fieldset', ['legend' => __('General Information')]);
        if ($model->getId()) {
            $fieldSet->addField('id', 'hidden', ['name' => 'id']);
        }

        $fieldSet->addField(
            'fee_label',
            'text',
            [
                'label' => __('Fee Label'),
                'title' => __('Fee Label'),
                'required' => true,
                'name' => 'fee_label',
            ]
        );

        $fieldSet->addField(
            'fee_type',
            'select',
            [
                'label' => __('Fee Type'),
                'title' => __('Fee Type'),
                'required' => true,
                'name' => 'fee_type',
                'options' => $this->_feeType->getOptionArray(),
            ]
        );

        $fieldSet->addField(
            'fee_amount',
            'text',
            [
                'label' => __('Fee Amount'),
                'title' => __('Fee Amount'),
                'required' => true,
                'name' => 'fee_amount',
            ]
        );

        $toDisplay = $fieldSet->addField(
            'area_to_display',
            'select',
            [
                'required' => true,
                'label' => __('Add Fee To'),
                'title' => __('Add Fee To'),
                'name' => 'area_to_display',
                'options' => $this->_feeOption->getOptionArray(),
                'disabled' => $isElementDisabled
            ]
        );

        $categoryList = $fieldSet->addField(
            'categorylist',
            'multiselect',
            [
                'name' => 'categorylist[]',
                'label' => __('Category List'),
                'title' => __('Category List'),
                'required' => true,
                'values' => $this->_categorylist->toOptionArray(),
            ]
        );

        $paymentMethod = $fieldSet->addField(
            'payment_method',
            'multiselect',
            [
                'name' => 'payment_method[]',
                'label' => __('Payment method'),
                'title' => __('Payment method'),
                'required' => true,
                'values' => $this->_paymentMethod->toOptionArray(),
            ]
        );

        $applyForAll = $fieldSet->addField(
            'apply_for_all_product',
            'select',
            [
                'name' => 'apply_for_all_product',
                'label' => __('Fee Apply For All Product'),
                'title' => __('Fee Apply For All Product'),
                'required' => false,
                'note' => 'Default, Manually set fee in product.',
                'options' => $this->booleanValue->getOptionArray()
            ]
        );

        $productSku = $fieldSet->addField(
            'product_sku',
            'textarea',
            [
                'label' => __('Product SKU'),
                'title' => __('Product SKU'),
                'required' => false,
                'name' => 'product_sku',
				'note' => 'Please Enter Comma Separated Values (SKU1,SKU2,SKU3)',
            ]
        );
		
		if($this->helper->isOptionFeeEnabled())
		{
			$optionsSku = $fieldSet->addField(
				'option_sku',
				'text',
				[
					'label' => __('Options SKU'),
					'title' => __('Options SKU'),
					'required' => false,
					'name' => 'option_sku',
				]
			);
		} else {
			$optionsSku = $productSku;
		}
		
        $fieldSet->addField(
            'store_ids',
            'multiselect',
            [
                'name' => 'store_ids[]',
                'label' => __('Store Views'),
                'title' => __('Store Views'),
                'required' => true,
                'values' => $this->_systemStore->getStoreValuesForForm(false, true),
				'default' => 0,
            ]
        );

        $fieldSet->addField(
            'status',
            'select',
            [
                'required' => true,
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'status',
                'options' => $this->_status->getOptionArray(),
                'disabled' => $isElementDisabled
            ]
        );

        $fieldSet->addField(
            'priority',
            'text',
            [
                'label' => __('Priority'),
                'title' => __('Priority'),
                'required' => false,
                'name' => 'priority',
            ]
        );
		
		if($model->getData('store_ids') == ""){
			$model->setData('store_ids', 0);
		}
		
		if($model->getData('status') == ""){
			$model->setData('status', 1);
		}
		
		if($model->getData('priority') == ""){
			$model->setData('priority', 0);
		}
		
        $form->setValues($model->getData());
		
        $this->setForm($form);
		
		$refFieldPayment = $this->_fieldFactory->create(
			 ['fieldData' => ['value' => 'payment,payment_fee_on_product', 'separator' => ','], 'fieldPrefix' => '']
		);
		
		$refFieldProduct = $this->_fieldFactory->create(
			 ['fieldData' => ['value' => 'product,payment_fee_on_product', 'separator' => ','], 'fieldPrefix' => '']
		);
		
        $this->setChild(
            'form_after',
            $this->getLayout()->createBlock(
                'Magento\Backend\Block\Widget\Form\Element\Dependence'
            )->addFieldMap(
                $toDisplay->getHtmlId(),
                $toDisplay->getName()
            )
                // category
                ->addFieldMap(
                    $categoryList->getHtmlId(),
                    $categoryList->getName()
                )->addFieldDependence(
                    $categoryList->getName(),
                    $toDisplay->getName(),
                    'category'
                )
                // paymentMethod
                ->addFieldMap(
                    $paymentMethod->getHtmlId(),
                    $paymentMethod->getName()
                )->addFieldDependence(
                    $paymentMethod->getName(),
                    $toDisplay->getName(),
                    $refFieldPayment
                )
                // Product
                ->addFieldMap(
                    $applyForAll->getHtmlId(),
                    $applyForAll->getName()
                )->addFieldDependence(
                    $applyForAll->getName(),
                    $toDisplay->getName(),
                    'product'
                )->addFieldMap(
                    $optionsSku->getHtmlId(),
                    $optionsSku->getName()
                )->addFieldDependence(
                    $optionsSku->getName(),
                    $toDisplay->getName(),
                    'product'
                )->addFieldMap(
                    $productSku->getHtmlId(),
                    $productSku->getName()
                )->addFieldDependence(
                    $productSku->getName(),
                    $toDisplay->getName(),
                    $refFieldProduct
                )->addFieldDependence(
                    $productSku->getName(),
                    $applyForAll->getName(),
                    '0'
                )

        );

        return parent::_prepareForm();
    }


    /**
     * @return mixed
     */
    public function getPageTitle()
    {
        return $this->getExtraFee()->getId() ? __(
            "Edit Extra Fee '%1'",
            $this->escapeHtml($this->getExtraFee()->getTitle())
        ) : __('New Extra Fee');
    }

    /**
     * @return mixed
     */
    public function getTabLabel()
    {
        return __('Extra Fee Information');
    }

    /**
     * @return mixed
     */
    public function getTabTitle()
    {
        return __('Extra Fee Information');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }
}