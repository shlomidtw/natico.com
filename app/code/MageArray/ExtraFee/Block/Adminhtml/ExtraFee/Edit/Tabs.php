<?php
namespace MageArray\ExtraFee\Block\Adminhtml\ExtraFee\Edit;

/**
 * Class Tabs
 * @package MageArray\ExtraFee\Block\Adminhtml\ExtraFee\Edit
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     *
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('extrafee_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Extra Fee Information'));
    }

    /**
     *
     */
    protected function _prepareLayout()
    {
        $this->addTab(
            'main_section_general',
            [
                'label' => __('General Information'),
                'title' => __('General Information'),
                'content' => $this->getLayout()
                    ->createBlock(
                        'MageArray\ExtraFee\Block\Adminhtml\ExtraFee\Edit\Tab\General'
                    )
                    ->toHtml(),
                'active' => true
            ]
        );
    }
}
