<?php
namespace MageArray\ExtraFee\Block\Adminhtml\ExtraFee;
/**
 * Class Grid
 * @package MageArray\ExtraFee\Block\Adminhtml\ExtraFee
 */
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var
     */
    protected $_extrafeeFactory;

    /**
     * Grid constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \MageArray\ExtraFee\Model\ExtraFeeFactory $extraFeeFactory
     * @param \MageArray\ExtraFee\Model\Config\Source\Status $status
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \MageArray\ExtraFee\Model\ExtraFeeFactory $extraFeeFactory,
        \MageArray\ExtraFee\Model\Config\Source\Status $status,
        \MageArray\ExtraFee\Model\Config\Source\FeeType $feeType,
        \MageArray\ExtraFee\Model\Config\Source\FeeOption $feeOption,
        \MageArray\ExtraFee\Model\Config\Source\BooleanValue $booleanValue,
        array $data = []
    ) {
        $this->_extraFeeFactory = $extraFeeFactory;
        $this->_status = $status;
        $this->feeType = $feeType;
        $this->feeOption = $feeOption;
        $this->booleanValue = $booleanValue;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     *
     */
    protected function _construct()
    {

        parent::_construct();
        $this->setId('extrafeeGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * @return mixed
     */
    protected function _prepareCollection()
    {
        $collection = $this->_extraFeeFactory->create()->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return mixed
     */
    protected function _prepareColumns()
    {

        $this->addColumn(
            'id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
            ]
        );

        $this->addColumn(
            'fee_label',
            [
                'header' => __('Fee Label'),
                'index' => 'fee_label',
            ]
        );

		$store = $this->_getStore();
        $this->addColumn(
            'fee_amount',
			[
				'header' => __('Fee Amount'),
                'type' => 'currency',
                'currency_code' => $store->getBaseCurrency()->getCode(),
                'index' => 'fee_amount',
			]
        );

        $this->addColumn(
            'fee_type',
            [
                'header' => __('Fee Type'),
                'index' => 'fee_type',
				'type' => 'options',
                'options' => $this->feeType->getOptionArray()
            ]
        );

        $this->addColumn(
            'area_to_display',
            [
                'header' => __('Add Fee To'),
                'index' => 'area_to_display',
				'type' => 'options',
                'options' => $this->feeOption->getOptionArray()
            ]
        );

        /* $this->addColumn(
            'apply_for_all_product',
            [
                'header' => __('Fee Apply For All Product'),
                'index' => 'apply_for_all_product',
				'type' => 'options',
                'options' => $this->booleanValue->getOptionArray()
            ]
        ); */

        $this->addColumn(
            'status',
            [
                'header' => __('Status'),
                'index' => 'status',
                'type' => 'options',
                'options' => $this->_status->getOptionArray()
            ]
        );

        $this->addColumn(
            'action',
            [
                'header' => __('Action'),
                'index' => 'status',
                'type' => 'action',
                'getter' => 'getId',
                'width' => '20px',
                'actions' => [
                    [
                        'caption' => __('Edit'),
                        'url' => [
                            'base' => '*/*/edit',
                        ],
                        'field' => 'id'
                    ]
                ],
                'filter' => false,
                'sortable' => false
            ]
        );
        return parent::_prepareColumns();
    }
	
	protected function _getStore()
    {
        $storeId = (int)$this->getRequest()->getParam('store', 0);
        return $this->_storeManager->getStore($storeId);
    }

    /**
     * @param $row
     * @return mixed
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', ['id' => $row->getId()]);
    }

    /**
     * @return mixed
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', ['_current' => true]);
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('extrafee');
        $this->getMassactionBlock()->addItem('delete', [
            'label' => __('Delete'),
            'url' => $this->getUrl('*/*/massdelete', ['' => '']),
            'confirm' => __('Are you sure?')
        ]);
        $statuses = $this->_status->getOptionArray();
        //array_unshift($statuses, ['label' => '', 'value' => '']);
        $this->getMassactionBlock()->addItem('status', [
            'label' => __('Change status'),
            'url' => $this->getUrl('*/*/massstatus', ['_current' => true]),
            'additional' => [
                'visibility' => [
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => __('Status'),
                    'values' => $statuses
                ]
            ]
        ]);

        return $this;
    }
}
