<?php
namespace MageArray\ExtraFee\Block\Adminhtml\ExtraFee\Grid\Column;

use Magento\Backend\Block\Widget\Grid\Column;

/**
 * Class Statuses
 * @package MageArray\ExtraFee\Block\Adminhtml\ExtraFee\Grid\Column
 */
class Statuses extends Column
{
    /**
     * @return array
     */
    public function getFrameCallback()
    {
        return [$this, 'decorateStatus'];
    }

    /**
     * @param $value
     * @param $row
     * @return string
     */
    public function decorateStatus($value, $row)
    {
        if ($row->getStatus() == 1) {
            $cell = '<span class="grid-severity-notice"><span>' . $value . '</span></span>';
        } else if ($row->getStatus() == 2) {
            $cell = '<span class="grid-severity-critical"><span>' . $value . '</span></span>';
        } else {
            $cell = '<span class="grid-severity-notice"><span>' . $value . '</span></span>';
        }
        return $cell;
    }
}
