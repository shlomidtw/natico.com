<?php
namespace MageArray\ExtraFee\Block;

class ExtraFee extends \Magento\Framework\View\Element\Template
{
	protected $_registry;
	protected $maFees;
	protected $extraFeeFactory;
	
	public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
		\MageArray\ExtraFee\Model\MaFees $maFees,
		\MageArray\ExtraFee\Model\ExtraFeeFactory $extraFeeFactory,
        array $data = []
    ) {
        $this->_registry = $registry;
        $this->maFees = $maFees;
        $this->extraFeeFactory = $extraFeeFactory;
        parent::__construct($context, $data);
    }
	
	public function getAmountTitle($fee)
	{
		return $this->maFees->getAmountTitle($fee);
	}
	
	public function getProductFee()
	{
		if($this->_registry->registry('current_product'))
		{
			$product = $this->_registry->registry('current_product');
			$ids = $this->maFees->getFees($product);
			$collection = $this->extraFeeFactory->create()->getCollection();
			$collection->addFieldToFilter('id', ['in' => $ids]);
			return $collection;
		}
	}
	
	public function getProductOptionFee()
	{
		if($this->_registry->registry('current_product'))
		{
			$fees = $this->maFees->getProductOptionFee($this->_registry->registry('current_product'));
			return json_encode($fees, true);
		}
	}
}