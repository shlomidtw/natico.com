<?php
namespace MageArray\ExtraFee\Block\Sales\Order;

/**
 * Class MaExtraFee
 * @package MageArray\ExtraFee\Block\Sales\Order
 */
class MaExtraFee extends \Magento\Framework\View\Element\Template
{
    /**
     * @var
     */
    protected $_order;
    /**
     * @var
     */
    protected $_source;

    /**
     * MaExtraFee constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \MageArray\ExtraFee\Model\SalesExtraFeeFactory $salesExtraFeeFactory
     * @param \MageArray\ExtraFee\Helper\Data $helper
     * @param \Magento\Sales\Helper\Admin $salesAdminHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \MageArray\ExtraFee\Model\SalesExtraFeeFactory $salesExtraFeeFactory,
        \MageArray\ExtraFee\Helper\Data $helper,
        \Magento\Sales\Helper\Admin $salesAdminHelper,
        array $data = []
    ) {
        $this->salesExtraFeeFactory = $salesExtraFeeFactory;
        $this->dataHelper = $helper;
        $this->_salesAdminHelper = $salesAdminHelper;
        parent::__construct($context, $data);
    }

    /**
     * @return mixed
     */
    public function getSalesFeeCollection()
    {
        $parent = $this->getParentBlock();
        $this->_order = $parent->getOrder();

        $salesFeeCollection = $this->salesExtraFeeFactory->create()->getCollection();
        $salesFeeCollection->addFieldToFilter('order_id', $this->_order->getId());
        $salesFeeCollection->addFieldToFilter('area_to_display', ['order', 'payment','payment_fee_on_product']);

        return $salesFeeCollection;
    }

    /**
     * @return $this
     */
    public function initTotals()
    {
        $parent = $this->getParentBlock();
        $this->_source = $parent->getSource();
        $total = new \Magento\Framework\DataObject(['code' => 'maextrafee', 'block_name' => $this->getNameInLayout()]);
        $parent->addTotal($total);
        return $this;
    }

    /**
     * @param $amount
     * @param $baseAmount
     * @return mixed
     */
    public function displayAmount($amount, $baseAmount)
    {
        $parent = $this->getParentBlock();
        return $this->_salesAdminHelper->displayPrices($parent->getSource(), $baseAmount, $amount, false, '<br />');
    }

    /**
     * @return mixed
     */
    public function getFeeTitle()
    {
        return $this->dataHelper->getOrderFeeTitle();
    }

    /**
     * @return mixed
     */
    public function getMaExtraFee()
    {
        return $this->_order->getMaExtraFee();
    }

    /**
     * @return mixed
     */
    public function getBaseMaExtraFee()
    {
        return $this->_order->getBaseMaExtraFee();
    }

    /**
     * @return mixed
     */
    public function getLabelProperties()
    {
        return $this->getParentBlock()->getLabelProperties();
    }

    /**
     * @return mixed
     */
    public function getValueProperties()
    {
        return $this->getParentBlock()->getValueProperties();
    }
}