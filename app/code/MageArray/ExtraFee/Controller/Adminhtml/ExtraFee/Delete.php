<?php
namespace MageArray\ExtraFee\Controller\Adminhtml\ExtraFee;

/**
 * Class Delete
 * @package MageArray\ExtraFee\Controller\Adminhtml\ExtraFee
 */
class Delete extends \MageArray\ExtraFee\Controller\Adminhtml\ExtraFee
{

    /**
     * @return mixed
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->_extraFeeFactory->create()->load($id);
                $model->delete();
                $this->messageManager->addSuccess(
                    __(
                        'The Fee has been deleted.'
                    )
                );
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath(
                    '*/*/edit',
                    ['id' => $id]
                );
            }
        }
        $this->messageManager->addError(__('We can\'t find a Fee to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
