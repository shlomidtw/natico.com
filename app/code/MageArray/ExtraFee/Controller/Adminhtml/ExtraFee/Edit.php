<?php
namespace MageArray\ExtraFee\Controller\Adminhtml\ExtraFee;

/**
 * Class Edit
 * @package MageArray\ExtraFee\Controller\Adminhtml\ExtraFee
 */
class Edit extends \MageArray\ExtraFee\Controller\Adminhtml\ExtraFee
{
    /**
     *
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->_extraFeeFactory->create();
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(
                    __('This Extra Fee no longer exists.')
                );
                $this->_redirect('*/*/');
                return;
            }
        }
        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        $this->_coreRegistry->register('extrafee', $model);
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }

    /**
     * @return mixed
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(
            'MageArray_ExtraFee::ma_extra_fee'
        );
    }
}
