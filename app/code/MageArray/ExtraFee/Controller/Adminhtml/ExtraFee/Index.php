<?php
namespace MageArray\ExtraFee\Controller\Adminhtml\ExtraFee;

/**
 * Class Index
 * @package MageArray\ExtraFee\Controller\Adminhtml\ExtraFee
 */
class Index extends \MageArray\ExtraFee\Controller\Adminhtml\ExtraFee
{
    /**
     * @return mixed
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('MageArray_ExtraFee::extrafee');
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Extra Fee'));
        return $resultPage;
    }
}
