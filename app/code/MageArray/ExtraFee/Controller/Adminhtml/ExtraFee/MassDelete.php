<?php
namespace MageArray\ExtraFee\Controller\Adminhtml\ExtraFee;

/**
 * Class MassDelete
 * @package MageArray\ExtraFee\Controller\Adminhtml\ExtraFee
 */
class MassDelete extends \MageArray\ExtraFee\Controller\Adminhtml\ExtraFee
{
    /**
     * @return mixed
     */
    public function execute()
    {
        $feeIds = $this->getRequest()->getParam('extrafee');
        if (!is_array($feeIds) || empty($feeIds)) {
            $this->messageManager->addError(__('Please select at least one Fee.'));
        } else {
            try {
				$deleteFees = $this->_extraFeeFactory->create()->getCollection()->addFieldToFilter('id', array('in' => $feeIds));
				$count = $deleteFees->count();
				$deleteFees->walk('delete');
                $this->messageManager->addSuccess(__('A total of %1 record(s) have been deleted.',$count));
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        return $this->resultRedirectFactory->create()->setPath('*/*/index');
    }
}
