<?php
namespace MageArray\ExtraFee\Controller\Adminhtml\ExtraFee;

/**
 * Class MassStatus
 * @package MageArray\ExtraFee\Controller\Adminhtml\ExtraFee
 */
class MassStatus extends \MageArray\ExtraFee\Controller\Adminhtml\ExtraFee
{
    /**
     * @return mixed
     */
    public function execute()
    {
        $feeIds = $this->getRequest()->getParam('extrafee');
        if (!is_array($feeIds) || empty($feeIds)) {
            $this->messageManager->addError(__('Please select Fee(s).'));
        } else {
            try {
                $status = (int)$this->getRequest()->getParam('status');
                foreach ($feeIds as $feeId) {
                    $feeModel = $this->_extraFeeFactory->create()->load($feeId);
                    $feeModel->setStatus($status)->save();
                }
                $this->messageManager->addSuccess(
                    __(
                        'A total of %1 record(s) have been updated.',
                        count($feeIds)
                    )
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        return $this->resultRedirectFactory->create()->setPath('*/*/index');
    }
}
