<?php
namespace MageArray\ExtraFee\Controller\Adminhtml\ExtraFee;

/**
 * Class NewAction
 * @package MageArray\ExtraFee\Controller\Adminhtml\ExtraFee
 */
class NewAction extends \MageArray\ExtraFee\Controller\Adminhtml\ExtraFee
{
    /**
     *
     */
    public function execute()
    {
        $model = $this->_extraFeeFactory->create();
        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        $this->_coreRegistry->register('extrafee', $model);
        $this->_view->loadLayout();
        $this->_view->getLayout()->initMessages();
        $this->_view->renderLayout();
    }
}
