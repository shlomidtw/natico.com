<?php
namespace MageArray\ExtraFee\Controller\Adminhtml\ExtraFee;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class Save
 * @package MageArray\ExtraFee\Controller\Adminhtml\ExtraFee
 */
class Save extends \MageArray\ExtraFee\Controller\Adminhtml\ExtraFee
{
    /**
     * @return mixed
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $model = $this->_extraFeeFactory->create();
            $id = $this->getRequest()->getParam('id');
           
            try {
				
				if ($id) {
					$model->load($id);
				}

                if (isset($data['store_ids']) && !empty($data['store_ids'])) {
                    $storeIds = implode(',', $data['store_ids']);
                    $data['store_ids'] = $storeIds;
                }
                if (isset($data['categorylist']) && !empty($data['categorylist'])) {
                    $categoryList = implode(',', $data['categorylist']);
                    $data['categorylist'] = $categoryList;
                }
                if (isset($data['payment_method']) && !empty($data['payment_method'])) {
                    $paymentMethod = implode(',', $data['payment_method']);
                    $data['payment_method'] = $paymentMethod;
                }
				
				$productSku = [];
                if (isset($data['product_sku']) && !empty($data['product_sku'])) {
                    $productSku = array_map('trim', explode(',', $data['product_sku']));
					$data['product_sku'] = implode(',',$productSku);
                }
				
				if (!isset($data['apply_for_all_product'])) {
                    $data['apply_for_all_product'] = '0';
                }
				
                $oldProductSku = explode(',', $model->getProductSku());
				$deleteSku = array_diff($oldProductSku, $productSku);
				$insertSku = $productSku;
				
				if(!empty($deleteSku) && $id && $deleteSku != '') {
					// remove attribute in product
					foreach ($deleteSku as $oldSku) {
						if($oldSku)
						{
							$product = $this->getProduct($oldSku);
							if ($product) {
								$oldValue = $product->getMaExtraFeeAttribute();
								$oldValueArr = explode(',', $oldValue);
								for ($i = 0; $i < count($oldValueArr); $i++) {
									if ($oldValueArr[$i] == $id) {
										unset($oldValueArr[$i]);
									}
								}
								
								$product->setStoreId($product->getStoreId());
								$product->setData('ma_extra_fee_attribute', implode(',',$oldValueArr));
								$product->getResource()->saveAttribute($product, 'ma_extra_fee_attribute');
							} else {
								$this->messageManager->addError(
									'Product SKU - ' . $oldSku . ' not available.'
								);
							}
						}
					}
				}
                
                $model->setData($data);
                $model->save();
				$id = $model->getId();
				
				if (!empty($insertSku) && $id) {
					// add attribute in product
					$finalSku = [];
					foreach ($insertSku as $newSku) {
						$product = $this->getProduct($newSku);
						if ($product) {
							$oldValue = $product->getMaExtraFeeAttribute();
							$oldValueArr = explode(',', $oldValue);
							$oldValueArr[] = $id;
							$attrValue = array_unique($oldValueArr);
							
							$product->setStoreId($product->getStoreId());
							$product->setData('ma_extra_fee_attribute', implode(',',$attrValue));
							$product->getResource()->saveAttribute($product, 'ma_extra_fee_attribute');
							
							$finalSku[] = $newSku;
							
						} else {
							$this->messageManager->addError('Product SKU - ' . $newSku . ' not available.');
						}
					}
					
					if(!empty($finalSku))
					{
						$model->setProductSku(implode(',', $finalSku))->save();
					}
				}
				
                $this->messageManager->addSuccess(
                    __('The ExtraFee has been saved.')
                );
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath(
                        '*/*/edit', ['id' => $model->getId(),
                            '_current' => true]
                    );
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
			
            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath(
                '*/*/edit',
                ['id' => $this->getRequest()
                    ->getParam('id')]
            );
        }
        return $resultRedirect->setPath('*/*/');
    }
}
