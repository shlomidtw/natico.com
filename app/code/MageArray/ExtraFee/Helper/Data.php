<?php
namespace MageArray\ExtraFee\Helper;

/**
 * Class Data
 * @package MageArray\ExtraFee\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Extra fee config path
     */

    const MA_EXTRA_FEE_IS_ENABLED = 'ma_extra_fee/general/enable';
    /**
     *
     */
    const FEE_PER_UNIT = 'ma_extra_fee/general/fee_per_unit';
    /**
     *
     */
    const ORDER_FEE_TITLE = 'ma_extra_fee/general/order_fee_title';
	/**
     *
     */
    const PRODUCT_FEE_TITLE = 'ma_extra_fee/general/product_fee_title';
    /**
     *
     */
    const SHOW_CART_COLUMN = 'ma_extra_fee/general/show_cart_column';
    /**
     *
     */
    const CART_COLUMN_TITLE = 'ma_extra_fee/general/cart_column_title';
	/**
     *
     */
    const FEE_TOTAL_COLUMN = 'ma_extra_fee/general/fee_total_column';
	/**
     *
     */
    const TOTAL_COLUMN_TITLE = 'ma_extra_fee/general/total_column_title';
	/**
     *
     */
    const CUSTOM_OPTION_FEE = 'ma_extra_fee/general/custom_option_fee';
	/**
     *
     */
    const FEE_INCLUDES_TAX = 'ma_extra_fee/general/fee_includes_tax';/**
     /**
	 *
     */
    const FEE_SUMMARY = 'ma_extra_fee/general/fee_summary';/**
	 *
     */
    const FEE_TAX_CLASS = 'ma_extra_fee/general/fee_tax_class';
    const APPLY_TAX = 'ma_extra_fee/general/apply_tax';

    /**
     * Extra fee CODE
     */

    const MA_EXTRA_FEE_CODE = 'ma_extra_fee';
    /**
     *
     */
    const BASE_MA_EXTRA_FEE_CODE = 'base_ma_extra_fee';

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;
    /**
     * @var
     */
    protected $_customerViewHelper;

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\ObjectManagerInterface $objectManager,
		\Magento\Tax\Model\Calculation $calculation,
		\Magento\Tax\Model\TaxCalculation $taxCalculation
    ) {
        parent::__construct($context);
        $this->_storeManager = $storeManager;
        $this->_objectManager = $objectManager;
        $this->calculation = $calculation;
        $this->taxCalculation = $taxCalculation;
    }

    /**
     * @return mixed
     */
    public function getTaxRate()
    {
		if($this->isFeeIncludesTax())
		{
			return $this->taxCalculation->getDefaultCalculatedRate($this->getFeeTaxClass());
		}
    }
	
	public function getIncTaxAmount($amount, $taxRate = false)
	{
		if($this->isFeeIncludesTax())
		{
			if(!$taxRate)
			{
				$taxRate = $this->getTaxRate();
			}
			$amount += $this->calcTaxAmount($amount, $taxRate);
		}
		return $amount;
	}
	
	/**
     * @return mixed
     */
    public function calcTaxAmount($price, $taxRate, $priceIncludeTax = false, $round = true)
    {
        return $this->calculation->calcTaxAmount($price, $taxRate, $priceIncludeTax, $round);
    }
	
    /**
     * @return mixed
     */
    public function getCurrentStoreId()
    {
        return $this->_storeManager->getStore()->getStoreId();
    }

    /**
     * @return mixed
     */
    public function isModuleEnabled()
    {
        return $this->scopeConfig->getValue(
            self::MA_EXTRA_FEE_IS_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    }

    /**
     * @return mixed
     */
    public function getFeePerUnit()
    {
        return $this->scopeConfig->getValue(
            self::FEE_PER_UNIT,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    }

    /**
     * @return mixed
     */
    public function getProductFeeTitle()
    {
		if(!$this->isModuleEnabled()) { return false; }
        return $this->scopeConfig->getValue(
            self::PRODUCT_FEE_TITLE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    }
	
	/**
     * @return mixed
     */
    public function getOrderFeeTitle()
    {
		if(!$this->isModuleEnabled()) { return false; }
        return $this->scopeConfig->getValue(
            self::ORDER_FEE_TITLE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    }

    /**
     * @return mixed
     */
    public function getShowCartColumn()
    {
		if(!$this->isModuleEnabled()) { return false; }
        return $this->scopeConfig->getValue(
            self::SHOW_CART_COLUMN,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    }

    /**
     * @return mixed
     */
    public function getCartColumnTitle()
    {
		if(!$this->isModuleEnabled()) { return false; }
        return $this->scopeConfig->getValue(
            self::CART_COLUMN_TITLE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    }
	/**
     * @return mixed
     */
    public function getShowFeeTotalColumn()
    {
		if(!$this->isModuleEnabled()) { return false; }
        return $this->scopeConfig->getValue(
            self::FEE_TOTAL_COLUMN,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    }
	/**
     * @return mixed
     */
    public function getTotalColumnTitle()
    {
		if(!$this->isModuleEnabled()) { return false; }
        return $this->scopeConfig->getValue(
            self::TOTAL_COLUMN_TITLE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    } 
	
	/**
     * @return mixed
     */
	public function isOptionFeeEnabled()
    {
		if(!$this->isModuleEnabled()) { return false; }
        return $this->scopeConfig->getValue(
            self::CUSTOM_OPTION_FEE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    }
	
	/**
     * @return mixed
     */
	public function isFeeIncludesTax()
    {
		if(!$this->isModuleEnabled()) { return false; }
        return $this->scopeConfig->getValue(
            self::FEE_INCLUDES_TAX,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    }
	
	/**
     * @return mixed
     */
	public function showFeeSummary()
    {
		if(!$this->isModuleEnabled()) { return false; }
		if(!$this->applyTax()) { return false; }
        return $this->scopeConfig->getValue(
            self::FEE_SUMMARY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    }
	/**
     * @return mixed
     */
	public function getFeeTaxClass()
    {
		
		if(!$this->isModuleEnabled()) { return false; }
		if(!$this->applyTax()) { return false; }
        return $this->scopeConfig->getValue(
            self::FEE_TAX_CLASS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    }
	
	/**
     * @return mixed
     */
	public function applyTax()
    {
		if(!$this->isModuleEnabled()) { return false; }
        return $this->scopeConfig->getValue(
            self::APPLY_TAX,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getCurrentStoreId()
        );
    }
	
	/**
     * @return mixed
     */
	public function getM2Version()
    {
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$productMetadata = $objectManager->get('Magento\Framework\App\ProductMetadataInterface');
		return $productMetadata->getVersion();
    }
}
