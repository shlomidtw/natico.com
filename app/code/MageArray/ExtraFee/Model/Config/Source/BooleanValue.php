<?php
namespace MageArray\ExtraFee\Model\Config\Source;

/**
 * Class BooleanValue
 * @package MageArray\ExtraFee\Model\Config\Source
 */
class BooleanValue
{
    /**#@+
     * ExtraFee Status values
     */
    const STATUS_ENABLED = 1;

    /**
     *
     */
    const STATUS_DISABLED = 0;

    /**#@-*/

    /**
     * Retrieve Visible Status Ids
     *
     * @return int[]
     */
    public function getVisibleStatusIds()
    {
        return [self::STATUS_ENABLED];
    }

    /**
     * @return array
     */
    public static function getOptionArray()
    {
        return [self::STATUS_ENABLED => __('Yes'), self::STATUS_DISABLED => __('No')];
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $result = [];

        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    /**
     * @param $optionId
     * @return mixed|null
     */
    public function getOptionText($optionId)
    {
        $options = self::getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
}
