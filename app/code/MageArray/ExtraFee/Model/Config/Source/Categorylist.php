<?php
namespace MageArray\ExtraFee\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Categorylist
 * @package MageArray\ExtraFee\Model\Config\Source
 */
class Categorylist implements ArrayInterface
{
    /**
     * @var \Magento\Catalog\Helper\Category
     */
    protected $categoryHelper;

    /**
     * Categorylist constructor.
     * @param \Magento\Catalog\Helper\Category $catalogCategory
     */
    public function __construct(\Magento\Catalog\Helper\Category $catalogCategory)
    {
        $this->categoryHelper = $catalogCategory;
    }

    /*
     * Return categories helper
     */

    /**
     * @param bool $sorted
     * @param bool $asCollection
     * @param bool $toLoad
     * @return mixed
     */
    public function getStoreCategories($sorted = false, $asCollection = false, $toLoad = true)
    {
        return $this->categoryHelper->getStoreCategories($sorted, $asCollection, $toLoad);
    }

    /*  
     * Option getter
     * @return array
     */
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $arr = $this->toArray();
        $ret = [];

        foreach ($arr as $key => $value) {

            $ret[] = [
                'value' => $key,
                'label' => $value
            ];
        }

        return $ret;
    }

    /*
     * Get options in "key-value" format
     * @return array
     */
    /**
     * @return array
     */
    public function toArray()
    {
        $categories = $this->getStoreCategories(true, true, true);
        $categoriesList = [];
        foreach ($categories as $category) {

            $categoriesList[$category->getEntityId()] = __($category->getName());
        }

        return $categoriesList;
    }

}
