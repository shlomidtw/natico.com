<?php
namespace MageArray\ExtraFee\Model\Config\Source;

/**
 * Class FeeOption
 * @package MageArray\ExtraFee\Model\Config\Source
 */
class FeeOption
{
    /**#@+
     * FeeOption values
     */

    /**
     *
     */
    const TYPE_PRODUCT = 'product';
    /**
     *
     */
    const TYPE_CATEGORY = 'category';
    /**
     *
     */
    const TYPE_PAYMENT = 'payment';
    /**
     *
     */
    const TYPE_ORDER = 'order';
	/**
     *
     */
    const PAYMENT_FEE_ON_PRODUCT = 'payment_fee_on_product';

    /**#@-*/

    public static function getOptionArray()
    {
        return [
            self::TYPE_PRODUCT => __('Product'),
            self::TYPE_CATEGORY => __('Category'),
            self::TYPE_PAYMENT => __('Payment'),
            self::TYPE_ORDER => __('Order'),
            self::PAYMENT_FEE_ON_PRODUCT => __('Payment Fee On Product'),
        ];
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $result = [];

        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    /**
     * @param $optionId
     * @return mixed|null
     */
    public function getOptionText($optionId)
    {
        $options = self::getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
}
