<?php
namespace MageArray\ExtraFee\Model\Config\Source;

/**
 * Class FeeType
 * @package MageArray\ExtraFee\Model\Config\Source
 */
class FeeType
{
    /**#@+
     * ExtraFee Type values
     */
    const TYPE_FIX = 'fix';

    /**
     *
     */
    const TYPE_PERCENTAGE = 'percentage';

    /**#@-*/

    public static function getOptionArray()
    {
        return [self::TYPE_FIX => __('Fix Amount'), self::TYPE_PERCENTAGE => __('Percentage')];
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $result = [];

        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    /**
     * @param $optionId
     * @return mixed|null
     */
    public function getOptionText($optionId)
    {
        $options = self::getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
}
