<?php
namespace MageArray\ExtraFee\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class Options
 * @package MageArray\ExtraFee\Model\Config\Source
 */
class Options extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * @var
     */
    protected $helper;
    /**
     * @var \MageArray\ExtraFee\Model\MaFees
     */
    protected $maFees;

    /**
     * Options constructor.
     * @param \MageArray\ExtraFee\Helper\Data $helper
     * @param \MageArray\ExtraFee\Model\MaFees $maFees
     */
    public function __construct(
        \MageArray\ExtraFee\Helper\Data $helper,
        \MageArray\ExtraFee\Model\MaFees $maFees
    ) {
        $this->dataHelper = $helper;
        $this->maFees = $maFees;
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $collection = $this->maFees->getFeeCollection();
        $collection->addFieldToFilter('area_to_display', ['in'=>['product','payment_fee_on_product']]);
        $collection->addFieldToFilter('apply_for_all_product', 0);

        $this->_options = [];
        $this->_options[] = ['label' => '-- none --', 'value' => '0'];
        foreach ($collection as $fees) {
            if ($fees->getFeeType() == 'percentage') {
                $label = $fees->getFeeLabel() . ' ' . $fees->getFeeAmount() . '%';
            } else {
                $label = $fees->getFeeLabel() . ' ' . $this->maFees->getCurrencySymbol() . $fees->getFeeAmount();
            }

            $this->_options[] = [
                'label' => __($label),
                'value' => $fees->getId()
            ];
        }
        return $this->_options;
    }

    /**
     * @param $value
     * @return bool
     */
    public function getOptionText($value)
    {
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    }

    /**
     * @return array
     */
    public function getFlatColumns()
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        return [
            $attributeCode => [
                'unsigned' => false,
                'default' => null,
                'extra' => null,
                //'type' => Table::TYPE_VARCHAR,
                'type' => 'VARCHAR',                
                'nullable' => true,
                'comment' => 'Custom Attribute Options  ' . $attributeCode . ' column',
            ],
        ];
    }
}
