<?php
namespace MageArray\ExtraFee\Model\Config\Source;

use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Payment\Model\Config;

/**
 * Class Paymentmethod
 * @package MageArray\ExtraFee\Model\Config\Source
 */
class Paymentmethod extends \Magento\Framework\DataObject
    implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var ScopeConfigInterface
     */
    protected $_appConfigScope;
    /**
     * @var Config
     */
    protected $_paymentModelConfig;

    /**
     * @param ScopeConfigInterface $appConfigScope
     * @param Config $paymentModelConfig
     */
    public function __construct(
		\Magento\Payment\Model\PaymentMethodList $paymentMethodList,
        ScopeConfigInterface $appConfigScope,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\Api\ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        Config $paymentModelConfig
    ) {

        $this->_paymentMethodList = $paymentMethodList;
        $this->_appConfigScope = $appConfigScope;
		$this->_storeManager = $storeManager;
        $this->_paymentModelConfig = $paymentModelConfig;
		$this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }
	
	public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
		$methods = [];
		$methodList = $this->_paymentMethodList->getActiveList($this->getStoreId());
		foreach( $methodList as $_method )
		{
			$paymentTitle = $this->_appConfigScope->getValue('payment/' . $_method->getCode() . '/title');
			$methods[$_method->getCode()] = [
                'label' => $paymentTitle,
                'value' => $_method->getCode()
            ];
		}
		
        /* $payments = $this->_paymentModelConfig->getActiveMethods();
        foreach ($payments as $paymentCode => $paymentModel) {
			
            $paymentTitle = $this->_appConfigScope
                ->getValue('payment/' . $paymentCode . '/title');
            $methods[$paymentCode] = [
                'label' => $paymentTitle,
                'value' => $paymentCode
            ];
        } */
		
        return $methods;
    }
}
