<?php
namespace MageArray\ExtraFee\Model;

/**
 * Class ExtraFee
 * @package MageArray\ExtraFee\Model
 */
class ExtraFee extends \Magento\Framework\Model\AbstractModel
{
    /**
     *
     */
    public function _construct()
    {
        $this->_init('MageArray\ExtraFee\Model\ResourceModel\ExtraFee');
    }
}
