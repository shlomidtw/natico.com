<?php
namespace MageArray\ExtraFee\Model;

/**
 * Class MaFees
 * @package MageArray\ExtraFee\Model
 */
class MaFees extends \Magento\Framework\Model\AbstractModel {
    /**
     * @var ExtraFeeFactory
     */
    protected $extraFeeFactory;
    protected $_request;
    protected $_logger;

    /**
     * MaFees constructor.
     * @param \MageArray\ExtraFee\Helper\Data $helper
     * @param ExtraFeeFactory $extraFeeFactory
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \MageArray\ExtraFee\Helper\Data $helper,
        \MageArray\ExtraFee\Model\ExtraFeeFactory $extraFeeFactory,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
		\Magento\Framework\App\RequestInterface $request,
		\Magento\Sales\Model\OrderFactory $orderFactory,
		\Magento\Catalog\Model\ProductFactory $_productloader,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Psr\Log\LoggerInterface $_logger
    ) {
        $this->dataHelper = $helper;
        $this->extraFeeFactory = $extraFeeFactory;
        $this->priceCurrency = $priceCurrency;
        $this->_storeManager = $storeManager;
        $this->_request = $request;
        $this->orderFactory = $orderFactory;
		$this->_productloader = $_productloader;
		$this->_logger = $_logger;
    }

	public function getLoadProduct($id)
    {
        return $this->_productloader->create()->load($id);
    }
	
    /**
     * @return mixed
     */
    public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }
	
	/**
     * @return mixed
     */
    public function isModuleEnabled()
    {
        return $this->dataHelper->isModuleEnabled();
    }

    /**
     * @return mixed
     */
    public function getFeeCollection()
    {
        $stores = [];
        $stores['in'] = [0, $this->getStoreId()];

        $collection = $this->extraFeeFactory->create()->getCollection();
        $collection->addFieldToFilter('status', 1);
        $collection->setOrder('priority', 'ASC');
        $collection->addFieldToFilter('store_ids', $stores);

        return $collection;
    }

    /**
     * @param $fees
     * @param $item
     * @return float|int
     */
    public function getFeeAmount($fees, $item)
    {
        if ($fees->getFeeType() == 'percentage') {
            $price = $item->getProduct()->getFinalPrice($item->getQty());
            $amount = $fees->getFeeAmount();
            return ($price * $amount) / 100;
        }
        return $fees->getFeeAmount();
    }

    /**
     * @return mixed
     */
    public function getCurrencySymbol()
    {
        return $this->priceCurrency->getCurrencySymbol();
    }

    /**
     * @param $fees
     * @return string
     */
    public function getAmountTitle($fees)
    {
        if ($fees->getFeeType() == 'percentage') {
            return __($fees->getFeeAmount() . "%");
        }
        return $this->getCurrencySymbol() . $fees->getFeeAmount();
    }

    /**
     * @return mixed
     */
    public function getOrderFees($allOreders=false)
    {
        $collection = $this->getFeeCollection();
		if($allOreders)
		{
			$collection->addFieldToFilter('area_to_display', ['order', 'payment','payment_fee_on_product']);
		} else {
			$collection->addFieldToFilter('area_to_display', ['order', 'payment']);
		}
        return $collection;
    }

    /**
     * @param $product
     * @return array
     */
    public function getFees($product)
    {
		$ids = [];
		
		if (!$this->dataHelper->isModuleEnabled())
		{
			return $ids;
		}
        
        $collection = $this->getFeeCollection();
        $collection->addFieldToFilter('area_to_display', ['category', 'product']);

        $cats = $product->getCategoryIds();
        foreach ($collection as $fee) {
            if (!empty($cats)) {
                foreach ($cats as $cid) {
                    if (in_array($cid, explode(',', $fee->getCategorylist()))) {
                        $ids[] = $fee->getId();
                    }
                }
            }

            if ($fee->getApplyForAllProduct()) {
                $ids[] = $fee->getId();
            }
        }
		
		$maExtraFeeAttribute = $product->getData('ma_extra_fee_attribute');
        if ($maExtraFeeAttribute) {
            if ($product->getData('override_fee')) {
                $ids = [];
            }

            $attrIdFees = explode(',', $maExtraFeeAttribute);
            foreach ($attrIdFees as $id) {
                $extraFee = $this->getFeeById($id);
                if (!empty($extraFee)) {
                    if($extraFee->getAreaToDisplay() != 'payment_fee_on_product')
					{
						$ids[] = $extraFee->getId();
					}
                }
            }
        }
        return array_unique($ids);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getFeeById($id)
    {
        $extraFee = $this->extraFeeFactory->create()->load($id);
        if ($extraFee->getStatus()) {
            return $extraFee;
        }
    }

    /**
     * @param $item
     * @param $additionalOptions
     */
    public function applyItemCustomOption($item, $additionalOptions)
    {
        foreach ($item->getOptions() as $option) { 
            if ($option->getCode() != 'additional_options') {
                continue;
            }
			
			if (version_compare($this->dataHelper->getM2Version(), '2.2', '>='))
			{
				$child = json_decode($option->getValue(), true);
			} else {
				$child = unserialize($option->getValue());
			}
			
            if (!empty($child)) {	
				if (version_compare($this->dataHelper->getM2Version(), '2.2', '>='))
				{
					$option->setValue(json_encode($additionalOptions,true));
				} else {
					$option->setValue(serialize($additionalOptions));
				}
            }
        }
    }
	
    /**
     * @param $item
     */
    public function getAdditionalOptionsArray($ids,$product)
	{
		$fee = 0;
		$additionalOptions = [];
		if(!empty($ids))
		{
			$qty = $this->_request->getParam('qty') > 0 ? $this->_request->getParam('qty') : 1;
			foreach ($ids as $id) {
// 			    $this->_logger->info("ids : ".$id);
				$fees = $this->getFeeById($id);
				if ($fees) {
				    // ------------------------
				    //$fees =  {"id":"1","fee_label":"Setup Fee","fee_amount":"30.00","fee_type":"fix","area_to_display":"product","product_sku":"60-I360-BK-1","categorylist":null,"apply_for_all_product":"0","payment_method":null,"store_ids":"0","status":"1","priority":"0","option_sku":null} [] []
				    $fees->setFeeAmount($this->calAdditionalFeesByOptionValue($fees, $product));
// 				    $this->_logger->info("getFeeAmount : ".$fees->getFeeAmount());
				    //-------------------------
				    
					if ($fees->getFeeType() == 'percentage') {
						$amount = ($product->getFinalPrice($qty) * $fees->getFeeAmount()) / 100;
						$label = $fees->getFeeAmount() . '% ' . $fees->getFeeLabel();
					} else {
						$amount = $fees->getFeeAmount();
						$label = $fees->getFeeLabel();
					}

					$fee += $amount;
					$amount = $this->priceCurrency->getCurrencySymbol() . $amount;

					if ($fees) {
						$additionalOptions[] = [
							'feeid' => $fees->getId(),
							'code' => 'maoption',
							'label' => $label,
							'value' => $amount
						];
					}
				}
			}
		}
		
		return [$additionalOptions,$fee];
	}
	
	/**
	 * $fee from magearray_sales_order_fee Table
	 * $product from Item Quote
	 * @param object $fees
	 * @param object $product
	 * @return float $fee
	 */
	public function calAdditionalFeesByOptionValue($fees, $product){
	    $fee = 0;
// 	    $this->_logger->info("fees : ".json_encode($fees->getData()));
	    // //$fees =  {"id":"1","fee_label":"Setup Fee","fee_amount":"30.00","fee_type":"fix","area_to_display":"product","product_sku":"60-I360-BK-1","categorylist":null,"apply_for_all_product":"0","payment_method":null,"store_ids":"0","status":"1","priority":"0","option_sku":null} [] []
	    //---------magento 2 get product custom options value from quote -----------
	    $_customOptions = $product->getTypeInstance(true)->getOrderOptions($product);
	    if(array_key_exists('options', $_customOptions)){
	        foreach($_customOptions['options'] as $option){
// 	            $this->_logger->info("option: ".json_encode($option));
	            // {"label":"Imprint","value":"2 Color Imprint","print_value":"2 Color Imprint","option_id":"7","option_type":"drop_down","option_value":"15","custom_view":true}
	            
// 	            $this->_logger->info("option['value'] + fee_label : ".trim($option['value'])." + ".trim($fees['fee_label']));
	            if(trim($option['value']) == trim($fees['fee_label'])){
	                $fee =  $fees['fee_amount'];
	            }else{
	                $fee = 0;
	            }
	        }
	    }
	    //---------magento 2 get product custom options value from quote -----------
// 	    $this->_logger->info("fee: ".$fee);
	    return $fee;
	}
	
	public function getOptionFees($_item)
    {
		$ids = [];
		$options = $_item->getBuyRequest()->getData('options');
		if(!empty($options))
		{
			$types = ['drop_down','radio','checkbox','multiple'];
			foreach ($options as $key => $value) 
			{
				 $optionData = $_item->getProduct()->getOptionById($key);
				 if(in_array($optionData['type'], $types))
				 {
					 foreach ($optionData->getValues() as $v) 
					 {
						if ($v['option_type_id'] == $value) {
							$fee = $this->getFeeByOprionSku($v->getSku());
							if($fee->getId()){
								$ids[] = $fee->getId();
							}
						} 
						else if(is_array($value))
						{
							if(in_array($v['option_type_id'], $value))
							{
								$fee = $this->getFeeByOprionSku($v->getSku());
								if($fee->getId()){
									$ids[] = $fee->getId();
								}
							}
						}
					 }
				 } else {
					 if($value)
					 {
						$fee = $this->getFeeByOprionSku($optionData['sku']);
						if($fee->getId()){
							$ids[] = $fee->getId();
						}
					 }
				 }
			 }
		 }
		return $ids;
	}
	
	public function applyFeeOnItem($item)
    {         
	    

//         //---------magento 2 get product custom options value from quote -----------
//             $_customOptions = $item->getProduct()->getTypeInstance(true)->getOrderOptions($item->getProduct());
//             if(array_key_exists('options', $_customOptions)){
//                 foreach($_customOptions['options'] as $option){
//                     $this->_logger->info("After 3 : ".json_encode($option));
// //                     {
// //                         "label": "Imprint",
// //                         "value": "1 Color Imprint",
// //                         "print_value": "1 Color Imprint",
// //                         "option_id": "7",
// //                         "option_type": "drop_down",
// //                         "option_value": "14",
// //                         "custom_view": true
// //                     }
//                 }
//             }
//             //---------magento 2 get product custom options value from quote -----------
        
        $product = $item->getProduct();
        $ids = $this->getFees($product);
		
		if($this->dataHelper->isOptionFeeEnabled())
		{
			$optionFees = $this->getOptionFees($item);
			
			if(!empty($optionFees))
			{
				$ids = array_merge($ids,$optionFees);
			}
		}
		
// 		$this->_logger->info("optionFees : ".json_encode($ids));
// 		$this->_logger->info("optionFees2 : ".json_encode($product->getData()));
		
		list($additionalOptions, $fee) = $this->getAdditionalOptionsArray($ids,$product);
		$this->applyItemCustomOption($item, $additionalOptions);

		if ($fee > 0) {
// 		    $fee = 5;
			$qty = $item->getTotalQty();
			$price = $this->priceCurrency->round($item->getProduct()->getFinalPrice($item->getQty()));
				
			$customPrice = $price;
			$amount = $this->priceCurrency->round($price + ($fee / $qty));

			if ($this->dataHelper->getFeePerUnit()) {
				$customPrice = $this->priceCurrency->round($price + $fee);
				$amount = $price + $fee;
				$fee = $fee * $qty;
			}

			$item->setMaExtraFee($fee);
			$item->setBaseMaExtraFee($fee);
	
			$item->setCustomPrice($price);
			$item->setOriginalCustomPrice($amount);
			$item->getProduct()->setIsSuperMode(true);
		}
    }

    /**
     * @return array
     */
    public function getPaymentFeesArry()
    {
        $feeCollection = $this->getOrderFees(false);
        $arr = [];
        foreach ($feeCollection as $fees) {
            if ($fees->getAreaToDisplay() == 'payment' || $fees->getAreaToDisplay() == 'payment_fee_on_product') {
                $methods = explode(',', $fees->getPaymentMethod());
                foreach ($methods as $code) {
                    if ($fees->getFeeType() == 'percentage') {
                        $label = $fees->getFeeAmount() . '% ' . $fees->getFeeLabel();
                    } else {
                        $label = $fees->getFeeLabel() . ' ' .
                            $this->priceCurrency->getCurrencySymbol() .
                            $fees->getFeeAmount();
                    }

                    $arr[$code] = $label;
                }
            }
        }
        return $arr;
    }
	
	public function getFeeByOprionSku($sku)
	{
		$fees = $this->extraFeeFactory->create()->load($sku, 'option_sku');
		if($fees->getStatus())
		{
			return $fees;
		} else {
			return $this->extraFeeFactory->create([]);
		}
	}
	
	public function getProductOptionFee($product)
	{
		$options = $product->getOptions();
		$types = ['drop_down','radio','checkbox','multiple'];
		$otherOptions = [];
		foreach ($options as $o) {
			if(in_array($o->getType(), $types))
			{
				$optdata = [];
				foreach ($o->getValues() as $value) 
				{
					$fee = $this->getFeeByOprionSku($value->getSku());
					if($fee->getStatus())
					{
						$fee = $this->changePercentageValue($product, $fee);
					}
					$optdata[$value->getOptionTypeId()] = 
					[
						"type" => $value->getType(),
						"title" => $value->getDefaultTitle(),
						"price" => $value->getPrice(),
						"price_type" => $value->getDefaultPriceType(),
						"sku" => $value->getSku(),
						"option_fee" => $fee->getData(),
					];
				}
				if(isset($optdata) && !empty($optdata)) {
					$otherOptions[$o->getOptionId()] = $optdata;
				}				
				
			} else {
				$fee = $this->getFeeByOprionSku($o->getSku());
				if($fee->getStatus())
				{
					$fee = $this->changePercentageValue($product, $fee);
				}
				$otherOptions[$o->getOptionId()] =
				[
					"type" => $o->getType(),
					"title" => $o->getDefaultTitle(),
					"price" => $o->getPrice(),
					"price_type" => $o->getDefaultPriceType(),
					"sku" => $o->getSku(),
					"option_fee" => $fee->getData(),
				];
			}
		}
		return $otherOptions;
	}
	
	public function changePercentageValue($product, $fee)
	{
		if($product->getSpecialPrice())
		{
			$price = $product->getSpecialPrice();
		} else {
			$price = $product->getPrice();
		}
		$fee->setData('amount_title', $this->getAmountTitle($fee));
		if($fee->getFeeType() == 'percentage')
		{
			
			$totalFee = (($fee->getFeeAmount() * $price) / 100);
			$fee->setFeeAmount($totalFee);
			return $fee;
		} else {
			return $fee;
		}
	}
}
