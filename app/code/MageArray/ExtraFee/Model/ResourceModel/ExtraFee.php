<?php
namespace MageArray\ExtraFee\Model\ResourceModel;

/**
 * Class ExtraFee
 * @package MageArray\ExtraFee\Model\ResourceModel
 */
class ExtraFee extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     *
     */
    public function _construct()
    {
        $this->_init('magearray_extrafee', 'id');
    }
}
