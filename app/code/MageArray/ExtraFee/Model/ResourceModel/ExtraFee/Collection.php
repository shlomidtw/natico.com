<?php
namespace MageArray\ExtraFee\Model\ResourceModel\ExtraFee;

/**
 * Class Collection
 * @package MageArray\ExtraFee\Model\ResourceModel\ExtraFee
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     *
     */
    public function _construct()
    {
        $this->_init('MageArray\ExtraFee\Model\ExtraFee', 'MageArray\ExtraFee\Model\ResourceModel\ExtraFee');
    }
}
