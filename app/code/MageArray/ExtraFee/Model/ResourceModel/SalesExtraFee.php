<?php
namespace MageArray\ExtraFee\Model\ResourceModel;

/**
 * Class SalesExtraFee
 * @package MageArray\ExtraFee\Model\ResourceModel
 */
class SalesExtraFee extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     *
     */
    public function _construct()
    {
        $this->_init('magearray_sales_order_fee', 'entity');
    }
}
