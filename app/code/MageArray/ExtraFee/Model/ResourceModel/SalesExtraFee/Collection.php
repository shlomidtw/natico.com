<?php
namespace MageArray\ExtraFee\Model\ResourceModel\SalesExtraFee;

/**
 * Class Collection
 * @package MageArray\ExtraFee\Model\ResourceModel\SalesExtraFee
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     *
     */
    public function _construct()
    {
        $this->_init('MageArray\ExtraFee\Model\SalesExtraFee', 'MageArray\ExtraFee\Model\ResourceModel\SalesExtraFee');
    }
}
