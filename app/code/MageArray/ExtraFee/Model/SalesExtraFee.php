<?php
namespace MageArray\ExtraFee\Model;

/**
 * Class SalesExtraFee
 * @package MageArray\ExtraFee\Model
 */
class SalesExtraFee extends \Magento\Framework\Model\AbstractModel
{
    /**
     *
     */
    public function _construct()
    {
        $this->_init('MageArray\ExtraFee\Model\ResourceModel\SalesExtraFee');
    }
}
