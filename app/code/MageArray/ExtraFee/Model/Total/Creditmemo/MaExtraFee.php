<?php
namespace MageArray\ExtraFee\Model\Total\Creditmemo;

use Magento\Sales\Model\Order\Creditmemo;

/**
 * Class MaExtraFee
 * @package MageArray\ExtraFee\Model\Total\Creditmemo
 */
class MaExtraFee extends \Magento\Sales\Model\Order\Creditmemo\Total\AbstractTotal
{

    /**
     * @param Creditmemo $creditmemo
     * @return $this
     */
    public function collect(Creditmemo $creditmemo)
    {

        $order = $creditmemo->getOrder();
        $totalFeeAmount = $order->getMaExtraFee();
        $baseFeeAmount = $order->getBaseMaExtraFee();

		
		//$creditmemo->setTaxAmount($creditmemo->getTaxAmount() + 100);
        //$creditmemo->setBaseTaxAmount($creditmemo->getBaseTaxAmount() + 100);
		
        if ($totalFeeAmount > 0) {
            $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $totalFeeAmount);
        }
        if ($baseFeeAmount > 0) {
            $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $baseFeeAmount);
        }
        return $this;
    }

}
