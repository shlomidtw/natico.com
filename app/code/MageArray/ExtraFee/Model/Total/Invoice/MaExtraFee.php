<?php
namespace MageArray\ExtraFee\Model\Total\Invoice;

/**
 * Class MaExtraFee
 * @package MageArray\ExtraFee\Model\Total\Invoice
 */
class MaExtraFee extends \Magento\Sales\Model\Order\Invoice\Total\AbstractTotal
{
    /**
     * @param \Magento\Sales\Model\Order\Invoice $invoice
     * @return $this
     */
    public function collect(\Magento\Sales\Model\Order\Invoice $invoice)
    {
        $order = $invoice->getOrder();
		
        $totalFeeAmount = $order->getMaExtraFee();
        $baseFeeAmount = $order->getMaExtraFee();
		$orderId = $invoice->getOrderId();
		if($orderId)
		{
			if ($totalFeeAmount > 0) {
				$invoice->setGrandTotal($invoice->getGrandTotal() + $totalFeeAmount);
			}
			if ($baseFeeAmount > 0) {
				$invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $baseFeeAmount);
			}
		}else{
			$quoteId = $order->getQuoteId();
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$quote = $objectManager->create('Magento\Quote\Model\QuoteFactory');
			$quote = $quote->create()->load($quoteId);
		    $totalFeeAmount = $quote->getMaExtraFee();
			$baseFeeAmount = $quote->getMaExtraFee();
			if ($totalFeeAmount > 0) {
				$invoice->setGrandTotal($invoice->getGrandTotal() + $totalFeeAmount);
			}
			if ($baseFeeAmount > 0) {
				$invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $baseFeeAmount);
			}			
		}
        return $this;
    }
}
