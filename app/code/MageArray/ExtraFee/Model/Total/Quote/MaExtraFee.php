<?php
namespace MageArray\ExtraFee\Model\Total\Quote;

/**
 * Class MaExtraFee
 * @package MageArray\ExtraFee\Model\Total\Quote
 */
class MaExtraFee extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{
    /**
     * @var string
     */
    protected $code = \MageArray\ExtraFee\Helper\Data::MA_EXTRA_FEE_CODE;
    /**
     * @var string
     */
    protected $baseCode = \MageArray\ExtraFee\Helper\Data::BASE_MA_EXTRA_FEE_CODE;
    /**
     * @var string
     */
    protected $fix = \MageArray\ExtraFee\Model\Config\Source\FeeType::TYPE_FIX;

    /**
     * @var \Magento\Quote\Model\QuoteValidator|null
     */
    protected $quoteValidator = null;
    /**
     * @var \MageArray\ExtraFee\Helper\Data
     */
    protected $dataHelper;
    /**
     * @var
     */
    protected $extraFeeFactory;

    /**
     * MaExtraFee constructor.
     * @param \Magento\Quote\Model\QuoteValidator $quoteValidator
     * @param \MageArray\ExtraFee\Helper\Data $helper
     * @param \MageArray\ExtraFee\Model\MaFees $maFees
     */
    public function __construct(
        \Magento\Quote\Model\QuoteValidator $quoteValidator,
        \MageArray\ExtraFee\Helper\Data $helper,
		\Magento\Checkout\Model\Session $checkoutSession,
        \MageArray\ExtraFee\Model\MaFees $maFees
    ) {
        $this->quoteValidator = $quoteValidator;
        $this->dataHelper = $helper;
        $this->maFees = $maFees;
		$this->_checkoutSession = $checkoutSession;
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return $this
     */
    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        parent::collect($quote, $shippingAssignment, $total);
		$address = $shippingAssignment->getShipping()->getAddress();
		$subtotal = $address->getSubtotalInclTax();
		//$subtotal = $total->getSubtotal();
		
        if (!$this->dataHelper->isModuleEnabled()) {
            return $this;
        } 

        if ($subtotal == 0) {
            return $this;
        }
		
        $feeCollection = $this->maFees->getOrderFees(true); 
		
        if ($feeCollection->count() > 0) {
            $amount = 0;
			$method = $quote->getPayment()->getMethod();
			if(!$method) {
				$method = $this->_checkoutSession->getQuote()->getPayment()->getMethod();
			}
            foreach ($feeCollection as $fees) {
				
                if ($fees->getAreaToDisplay() == 'payment_fee_on_product') {
					continue;
				}
				
                if ($fees->getAreaToDisplay() == 'payment') {
                    $methodArr = explode(',', $fees->getPaymentMethod());
                    if (!in_array($method, $methodArr)) {
                        continue;
                    }
                }

                if ($fees->getFeeType() == 'percentage') {
                    $amount += ($subtotal * $fees->getFeeAmount()) / 100;
                } else {
                    $amount += $fees->getFeeAmount();
                }
            }
			
			foreach ($quote->getAllVisibleItems() as $item)
			{
				$id = $item->getProduct()->getMaExtraFeeAttribute();
				$ids = explode(',', $id);
				foreach($ids as $id)
				{
					$fees = $this->maFees->getFeeById($id);
					if($fees)
					{
						if($fees->getAreaToDisplay() != 'payment_fee_on_product')
						{
							continue;
						}
						$methodArr = explode(',', $fees->getPaymentMethod());
						if (in_array($method, $methodArr)) 
						{
							if ($fees->getFeeType() == 'percentage') {
								$price = $item->getProduct()->getFinalPrice($item->getQty());
								$feeAmount = ($price * $fees->getFeeAmount()) / 100;
							} else {
								$feeAmount = $fees->getFeeAmount();
							}
														
							if($this->dataHelper->getFeePerUnit())
							{
								$amount += ($item->getQty() * $feeAmount);
							} else {
								$amount += ($feeAmount);
							}
						}
					}
				}
			}
			//echo $subtotal .' - '.$amount; die;
            $total->setTotalAmount($this->code, $amount);
            $total->setBaseTotalAmount($this->baseCode, $amount);

            $quote->setMaExtraFee($amount);
            $quote->setBaseMaExtraFee($amount);

            $total->setMaExtraFee($amount);
            $total->setBaseMaExtraFee($amount);

            //$total->setGrandTotal($total->getGrandTotal() + $amount);
            //$total->setBaseGrandTotal($total->getBaseGrandTotal() + $amount);
        }

        return $this;
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return array|null
     */
    public function fetch(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        $result = null;
        if ($this->dataHelper->isModuleEnabled() && $total->getMaExtraFee() > 0) {
            $feeCollection = $this->maFees->getOrderFees(true);
            if ($feeCollection->count()) {
                $result = [
                    'code' => $this->code,
                    'title' => __(
                        $this->dataHelper
                            ->getOrderFeeTitle() ? $this->dataHelper->getOrderFeeTitle() : "Additional Fees"
                    ),
                    'value' => $total->getMaExtraFee()
                ];
            }
        }
        return $result;
    }
}