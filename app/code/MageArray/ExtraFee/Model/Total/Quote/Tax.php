<?php

namespace MageArray\ExtraFee\Model\Total\Quote;

use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote\Address as QuoteAddress;
use MageArray\ExtraFee\Helper\Data as DataHelper;
use Magento\Customer\Api\Data\AddressInterfaceFactory as CustomerAddressFactory;
use Magento\Customer\Api\Data\RegionInterfaceFactory as CustomerAddressRegionFactory;

class Tax extends \Magento\Tax\Model\Sales\Total\Quote\Tax
{
	
	 public function __construct(
        \Magento\Tax\Model\Config $taxConfig,
        \Magento\Tax\Api\TaxCalculationInterface $taxCalculationService,
        \Magento\Tax\Api\Data\QuoteDetailsInterfaceFactory $quoteDetailsDataObjectFactory,
        \Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory $quoteDetailsItemDataObjectFactory,
        \Magento\Tax\Api\Data\TaxClassKeyInterfaceFactory $taxClassKeyDataObjectFactory,
        CustomerAddressFactory $customerAddressFactory,
        CustomerAddressRegionFactory $customerAddressRegionFactory,
        \Magento\Tax\Helper\Data $taxData,
		\MageArray\ExtraFee\Model\MaFees $maFees,
		\Magento\Tax\Model\Calculation $calculation,
		\Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
		DataHelper $helper
    ) {
		$this->maFees = $maFees;
		$this->priceCurrency = $priceCurrency;
		$this->helper = $helper;
		$this->calculation = $calculation;
        parent::__construct(
            $taxConfig,
            $taxCalculationService,
            $quoteDetailsDataObjectFactory,
            $quoteDetailsItemDataObjectFactory,
            $taxClassKeyDataObjectFactory,
            $customerAddressFactory,
            $customerAddressRegionFactory,
			$taxData
        );
    }
	
    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        $this->clearValues($total);
        if (!$shippingAssignment->getItems()) {
            return $this;
        }

        $baseTaxDetails = $this->getQuoteTaxDetails($shippingAssignment, $total, true);
        $taxDetails = $this->getQuoteTaxDetails($shippingAssignment, $total, false);

        //Populate address and items with tax calculation results
        $itemsByType = $this->organizeItemTaxDetailsByType($taxDetails, $baseTaxDetails);
        if (isset($itemsByType[self::ITEM_TYPE_PRODUCT])) {
            $this->processProductItems($shippingAssignment, $itemsByType[self::ITEM_TYPE_PRODUCT], $total);
        }

        if (isset($itemsByType[self::ITEM_TYPE_SHIPPING])) {
            $shippingTaxDetails = $itemsByType[self::ITEM_TYPE_SHIPPING][self::ITEM_CODE_SHIPPING][self::KEY_ITEM];
            $baseShippingTaxDetails =
                $itemsByType[self::ITEM_TYPE_SHIPPING][self::ITEM_CODE_SHIPPING][self::KEY_BASE_ITEM];
            $this->processShippingTaxInfo($shippingAssignment, $total, $shippingTaxDetails, $baseShippingTaxDetails);
        }

        //Process taxable items that are not product or shipping
        $this->processExtraTaxables($total, $itemsByType);

        //Save applied taxes for each item and the quote in aggregation
        $this->processAppliedTaxes($total, $shippingAssignment, $itemsByType);

        if ($this->includeExtraTax()) {
            $total->addTotalAmount('extra_tax', $total->getExtraTaxAmount());
            $total->addBaseTotalAmount('extra_tax', $total->getBaseExtraTaxAmount());
        } 
		
		if($this->helper->isFeeIncludesTax())
		{
			$feeCollection = $this->maFees->getOrderFees();
			$includesTax = $this->helper->isFeeIncludesTax();
			
			$address = $shippingAssignment->getShipping()->getAddress();
			$taxRateRequest = $this->calculation->getRateRequest(
				$shippingAssignment->getShipping()->getAddress(),
				$shippingAssignment->getShipping()->getAddress(),
				$address->getQuote()->getCustomerTaxClassId(),
				$address->getQuote()->getStore()->getStoreId()
			);
			$taxRateRequest->setProductClassId($this->helper->getFeeTaxClass());
			$taxRate = $this->calculation->getRate($taxRateRequest);
	
			$totalTax = 0;
			$subtotal = $total->getSubtotal();
			foreach ($feeCollection as $fees) 
			{
				$percent = null;
				if ($fees->getAreaToDisplay() == 'payment') {
					$method = $quote->getPayment()->getMethod();
					$methodArr = explode(',', $fees->getPaymentMethod());
					if (!in_array($method, $methodArr)) {
						continue;
					}
				}

				if ($fees->getFeeType() == 'percentage') {
					$amount = $this->priceCurrency->round(($subtotal * $fees->getFeeAmount()) / 100);
					$percent = $fees->getFeeAmount() ? $fees->getFeeAmount().'%' : null;
					$label = $fees->getFeeLabel();
				} else {
					$amount = $this->priceCurrency->round($fees->getFeeAmount());
					$label = $fees->getFeeLabel();
				}
				
				if($includesTax)
				{
					$tax = $this->helper->calcTaxAmount($amount, $taxRate);
					$totalTax += $tax;
				}
			}
			
			$total->addTotalAmount('tax', $totalTax);
			$total->addBaseTotalAmount('tax', $totalTax);
			
		}
		return $this;
    }
}
