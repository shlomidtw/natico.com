<?php
namespace MageArray\ExtraFee\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class CartUpdatePost
 * @package MageArray\ExtraFee\Observer
 */
class CartUpdatePost implements ObserverInterface
{
    /**
     * @var \MageArray\ExtraFee\Helper\Data
     */
    protected $dataHelper;

    /**
     * CartUpdatePost constructor.
     * @param \MageArray\ExtraFee\Helper\Data $helper
     * @param \MageArray\ExtraFee\Model\MaFees $maFees
     */
    public function __construct(
        \MageArray\ExtraFee\Helper\Data $helper,
        \MageArray\ExtraFee\Model\MaFees $maFees
    ) {
        $this->dataHelper = $helper;
        $this->maFees = $maFees;
    }

    /**
     * @param EventObserver $observer
     */
    public function execute(EventObserver $observer)
    {
        if ($this->dataHelper->isModuleEnabled()) {
            $quote = $observer->getCart()->getQuote();
            foreach ($quote->getAllVisibleItems() as $item) {
                $this->maFees->applyFeeOnItem($item);
            }
        }
    }
}