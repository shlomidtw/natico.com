<?php
namespace MageArray\ExtraFee\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class CheckoutCartProductAddAfterObserver
 * @package MageArray\ExtraFee\Observer
 */
class CheckoutCartProductAddAfterObserver implements ObserverInterface
{
    /**
     * @var \MageArray\ExtraFee\Helper\Data
     */
    protected $dataHelper;

    /**
     * CheckoutCartProductAddAfterObserver constructor.
     * @param \MageArray\ExtraFee\Helper\Data $helper
     * @param \MageArray\ExtraFee\Model\MaFees $maFees
     */
    public function __construct(
        \MageArray\ExtraFee\Helper\Data $helper,
        \MageArray\ExtraFee\Model\MaFees $maFees
    ) {
        $this->dataHelper = $helper;
        $this->maFees = $maFees;
    }

    /**
     * @param EventObserver $observer
     */
    public function execute(EventObserver $observer)
    {
        if ($this->dataHelper->isModuleEnabled()) {
            $item = $observer->getQuoteItem();

            if ($item->getParentItem()) {
                $item = $item->getParentItem();
            }
            $this->maFees->applyFeeOnItem($item);
        }
    }
}
