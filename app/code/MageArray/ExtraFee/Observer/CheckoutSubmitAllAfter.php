<?php
namespace MageArray\ExtraFee\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\CouldNotSaveException;

/**
 * Class CheckoutSubmitAllAfter
 * @package MageArray\ExtraFee\Observer
 */
class CheckoutSubmitAllAfter implements ObserverInterface
{
    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @param RequestInterface $request
     */
    public function __construct(
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \MageArray\ExtraFee\Model\SalesExtraFeeFactory $salesExtraFeeFactory,
        \Magento\Sales\Model\Order $orderFacory,
        \MageArray\ExtraFee\Helper\Data $helper,
        \MageArray\ExtraFee\Model\MaFees $maFees
    ) {
        $this->salesExtraFeeFactory = $salesExtraFeeFactory;
        $this->quoteFactory = $quoteFactory;
        $this->orderFacory = $orderFacory;
        $this->dataHelper = $helper;
        $this->maFees = $maFees;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
		if(!$this->dataHelper->isModuleEnabled())
		{
			return;
		}

		$order = $observer->getEvent()->getOrder();
		$quote = $observer->getEvent()->getQuote();
		
		$newFeesArr = [];
		foreach ($quote->getAllVisibleItems() as $item) 
		{
			foreach ($item->getOptions() as $option) {
				if ($option->getCode() != 'additional_options') {
					continue;
				}
				
				if (version_compare($this->dataHelper->getM2Version(), '2.2', '>='))
				{
					$child = json_decode($option->getValue(), true);
				} else {
					$child = unserialize($option->getValue());
				}
				
				if (!empty($child)) {
					$this->checkOptionChildFeeId($child, $order, $quote, $item);
				}
			}
			
			$id = $item->getProduct()->getMaExtraFeeAttribute();
			$fees = $this->maFees->getFeeById($id);
			if($fees)
			{
				$fees->setQty($item->getQty());
				if(isset($newFeesArr[$fees->getId()]))
				{
					if(isset($newFeesArr[$fees->getId()]['qty']))
					{
						$oldQty = $newFeesArr[$fees->getId()]['qty'];
						$oldQty += $item->getQty();
						$newFeesArr[$fees->getId()]['qty'] = $oldQty;
					}
				} else {
					$newFeesArr[$fees->getId()] = $fees->getData();
				}
			}
		}

		$feesCollection = $this->maFees->getOrderFees();
		if(!empty($newFeesArr))
		{
			$feesCollection = array_merge($newFeesArr,$feesCollection->getData());
		} else {
			$feesCollection = $feesCollection->getData();
		}
		if (!empty($feesCollection)) {
			
			$subtotal = $quote->getSubtotal();
			foreach ($feesCollection as $fee) {
				if ($fee) 
				{
					$fees = new \Magento\Framework\DataObject();
					$fees->setData($fee);
					if ($fees->getAreaToDisplay() == 'payment' || $fees->getAreaToDisplay() == 'payment_fee_on_product') {
						$method = $quote->getPayment()->getMethod();
						$methodArr = explode(',', $fees->getPaymentMethod());
						if (!in_array($method, $methodArr)) {
							continue;
						}
					}

					try 
					{
						$model = $this->salesExtraFeeFactory->create();
						$model->setData($fees->getData());

						$model->setOrderId($order->getId());
						$model->setQuoteId($quote->getId());

						if ($fees->getFeeType() == 'percentage') {
							$feeAmount = ($subtotal * $fees->getFeeAmount()) / 100;
						} else {
							$feeAmount = $fees->getFeeAmount();
						}
						
						if($this->dataHelper->getFeePerUnit() && $fees->getQty())
						{
							$amount = ($fees->getQty() * $feeAmount);
						} else {
							$amount = ($feeAmount);
						}
						
						/* tax calculation */
						//$amount = $this->dataHelper->getIncTaxAmount($amount);
						
						$model->setRealAmount($amount);
						$model->save();

					} catch (\Exception $e) {
						throw new CouldNotSaveException(__($e));
					}
				}
			}
		}
    }
	
	public function checkOptionChildFeeId($child, $order, $quote, $item)
    {
        foreach ($child as $vl) {
            if (isset($vl['code']) == 'maoption') {
                if (isset($vl['feeid']) != '') {
                    $fees = $this->maFees->getFeeById($vl['feeid']);
                    if ($fees) {
                        try {
                            $model = $this->salesExtraFeeFactory->create();
                            $model->setData($fees->getData());
                            $model->setOrderId($order->getId());
                            $model->setQuoteId($quote->getId());
                            $model->setItemId($item->getId());
                            $amount = $this->maFees->getFeeAmount($fees, $item);
                            $model->setRealAmount($amount);
                            $model->save();
                        } catch (\Exception $e) {
                            throw new CouldNotSaveException(__($e));
                        }
                    }
                }
            }
        }
    }
}
