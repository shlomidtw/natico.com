<?php
namespace MageArray\ExtraFee\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
class FeePaypal implements ObserverInterface
{
	protected $_checkoutSession;
	protected $CustomercreditQuoteFactory;
	
	public function __construct(
		\Magento\Checkout\Model\Session $checkoutSession,
		\MageArray\ExtraFee\Helper\Data $helper
	) {
		$this->_checkoutSession = $checkoutSession;
		$this->_helper = $helper;
	}
    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
		$fee = $this->_checkoutSession->getQuote()->getMaExtraFee();
		if($fee > 0) {
			$feeTitle = $this->_helper->getOrderFeeTitle() ? $this->_helper->getOrderFeeTitle() : 'fees' ;
			$cart = $observer->getEvent()->getCart();
			$cart->addCustomItem($feeTitle, 1, $fee,'fees');
		}
		return $this;
	}
}