<?php
namespace MageArray\ExtraFee\Observer;

/**
 * Class OrderSaveBefore
 * @package MageArray\ExtraFee\Observer
 */
class FeePaypalPlace implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * OrderSaveBefore constructor.
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
	 * @param \MageArray\ExtraFee\Helper\Data $helper
     */
    public function __construct(
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\MageArray\ExtraFee\Helper\Data $helper
    ) {
        $this->quoteFactory = $quoteFactory;
        $this->scopeConfig = $scopeConfig;
		$this->dataHelper = $helper;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
		if($this->dataHelper->isModuleEnabled())
		{
			$order = $observer->getEvent()->getOrder();
			$quote = $this->quoteFactory->create()->load($order->getQuoteId());
			$feeAmount = $quote->getMaExtraFee();
			$baseFeeAmount = $quote->getBaseMaExtraFee();
			if ($feeAmount) {
				$order->setMaExtraFee($feeAmount);
			}
			if ($baseFeeAmount) {
				$order->setBaseMaExtraFee($baseFeeAmount);
			}
		}
        return $this;
    }
}
