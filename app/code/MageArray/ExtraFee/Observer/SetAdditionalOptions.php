<?php
namespace MageArray\ExtraFee\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

/**
 * Class SetAdditionalOptions
 * @package MageArray\ExtraFee\Observer
 */
class SetAdditionalOptions implements ObserverInterface
{
    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @param RequestInterface $request
     */
    public function __construct(
        RequestInterface $request,
        \MageArray\ExtraFee\Model\MaFees $maFees,
		\MageArray\ExtraFee\Helper\Data $helper
    ) {
        $this->_request = $request;
        $this->maFees = $maFees;
		$this->dataHelper = $helper;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (
            ($this->_request->getControllerName() == 'cart' && $this->_request->getActionName() == 'add') || 
			($this->_request->getControllerName() == 'cart' && $this->_request->getActionName() == 'updateItemOptions') ||
            ($this->_request->getFullActionName() == 'sales_order_reorder')
        ) {
			if($this->maFees->isModuleEnabled())
			{
				/* mafees_initialize need for add fees */
				$additionalOptions = [];
				$additionalOptions[] = [
							'feeid' => 'mafees_initialize',
							'code' => 'mafees_initialize',
							'label' => 'mafees_initialize',
							'value' => 'mafees_initialize'
						];
				if (version_compare($this->dataHelper->getM2Version(), '2.2', '>='))
				{
					$observer->getProduct()->addCustomOption('additional_options', json_encode($additionalOptions,true));
				} else {
					$observer->getProduct()->addCustomOption('additional_options', serialize($additionalOptions));
				}
				
			}
        }
    }
}
