<?php
namespace MageArray\ExtraFee\Plugin;

/**
 * Class AbstractMethod
 * @package MageArray\ExtraFee\Plugin
 */
class AbstractMethod
{
    /**
     * @var array
     */
    public $getFeeTitle = [];

    /**
     * AbstractMethod constructor.
     * @param \MageArray\ExtraFee\Model\MaFees $maFees
     * @param \MageArray\ExtraFee\Helper\Data $helper
     */
    public function __construct(
        \MageArray\ExtraFee\Model\MaFees $maFees,
		\MageArray\ExtraFee\Helper\Data $helper
    ) {
        $this->maFees = $maFees;
		$this->dataHelper = $helper;
    }

    /**
     * @param $subject
     * @param $title
     * @return string
     */
    public function afterGetTitle($subject, $title)
    {
		if($this->dataHelper->isModuleEnabled())
		{
			if (empty($this->getFeeTitle)) {
				$this->getFeeTitle = $this->maFees->getPaymentFeesArry();
			}

			if (isset($this->getFeeTitle[$subject->getCode()])) {
				return $title . ' (' . $this->getFeeTitle[$subject->getCode()] . ')';
			}
		}

        return $title;
    }
}
