<?php
namespace MageArray\ExtraFee\Plugin;

use Magento\Quote\Api\Data\TotalSegmentExtensionFactory;

/**
 * Class GrandTotalDetailsPlugin
 * @package MageArray\ExtraFee\Plugin
 */
class GrandTotalDetailsPlugin
{
	public $finalData = [];
    /**
     * @var \Magento\Tax\Api\Data\GrandTotalDetailsInterfaceFactory
     */
    protected $detailsFactory;

    /**
     * @var \Magento\Tax\Api\Data\GrandTotalRatesInterfaceFactory
     */
    protected $ratesFactory;

    /**
     * @var TotalSegmentFactory
     */
    protected $totalSegmentFactory;

    /**
     * @var string
     */
    protected $code = \MageArray\ExtraFee\Helper\Data::MA_EXTRA_FEE_CODE;

    /**
     * GrandTotalDetailsPlugin constructor.
     * @param \Magento\Tax\Api\Data\GrandTotalDetailsInterfaceFactory $detailsFactory
     * @param \Magento\Tax\Api\Data\GrandTotalRatesInterfaceFactory $ratesFactory
     * @param TotalSegmentFactory $totalSegmentFactory
     * @param \MageArray\ExtraFee\Model\MaFees $maFees
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Magento\Tax\Api\Data\GrandTotalDetailsInterfaceFactory $detailsFactory,
        \Magento\Tax\Api\Data\GrandTotalRatesInterfaceFactory $ratesFactory,
        TotalSegmentExtensionFactory $totalSegmentFactory,
        \MageArray\ExtraFee\Model\MaFees $maFees,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
		\MageArray\ExtraFee\Helper\Data $helper,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->detailsFactory = $detailsFactory;
        $this->ratesFactory = $ratesFactory;
        $this->totalSegmentFactory = $totalSegmentFactory;
        $this->maFees = $maFees;
        $this->priceCurrency = $priceCurrency;
		$this->dataHelper = $helper;
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * @param array $rates
     * @return array
     */
    protected function getRatesData($rates)
    {
        $taxRates = [];
        foreach ($rates as $rate) {
            $taxRate = $this->ratesFactory->create([]);
            $taxRate->setPercent($rate['percent']);
            $taxRate->setTitle($rate['title']);
            $taxRates[] = $taxRate;
        }
        return $taxRates;
    }

    /**
     * @param \Magento\Quote\Model\Cart\TotalsConverter $subject
     * @param \Closure $proceed
     * @param array $addressTotals
     * @return mixed
     */
    public function aroundProcess(
        \Magento\Quote\Model\Cart\TotalsConverter $subject,
        \Closure $proceed,
        array $addressTotals = []
    ) {
        $totalSegments = $proceed($addressTotals);

        if (!array_key_exists($this->code, $addressTotals)) {
            return $totalSegments;
        }
		
		//$address = $this->_checkoutSession->getQuote()->getShippingAddress();
		//$subtotal = $address->getSubtotalInclTax();
        $subtotal = $totalSegments['subtotal']['value'];
        $feeCollection = $this->maFees->getOrderFees(true);
        $this->setFinalDatas($subtotal, $feeCollection->getData(), true);
		
		$quote = $this->_checkoutSession->getQuote();
		$newFeesArr = [];
		foreach ($quote->getAllVisibleItems() as $item)
		{
			$id = $item->getProduct()->getMaExtraFeeAttribute();
			if($id)
			{
				$ids = explode(',', $id);
				foreach($ids as $id)
				{
					$fees = $this->maFees->getFeeById($id);
					if($fees)
					{
						if($fees->getAreaToDisplay() != 'payment_fee_on_product')
						{
							continue;
						}
						
						$fees->setQty($item->getQty());
						
						if ($fees->getFeeType() == 'percentage') {
							$price = $item->getProduct()->getFinalPrice($item->getQty());
							//$feeAmount = $this->priceCurrency->round(($price * $fees->getFeeAmount()) / 100);
							$feeAmount = ($price * $fees->getFeeAmount()) / 100;
						} else {
							$feeAmount = $fees->getFeeAmount();
						}
						
						if($this->dataHelper->getFeePerUnit())
						{
							$amount = ($fees->getQty() * $feeAmount);
						} else {
							$amount = ($feeAmount);
						}
						$fees->setItemPrice($amount);
						
						if(isset($newFeesArr[$fees->getId()]))
						{
							if(isset($newFeesArr[$fees->getId()]['qty']))
							{
								$oldQty = $newFeesArr[$fees->getId()]['qty'];
								$oldQty += $item->getQty();
								$newFeesArr[$fees->getId()]['qty'] = $oldQty;
							}
							
							if(isset($newFeesArr[$fees->getId()]['item_price']))
							{
								$oldPrce = $newFeesArr[$fees->getId()]['item_price'];
								$newFeesArr[$fees->getId()]['item_price'] = $amount + $oldPrce;
							}
						} else {
							$newFeesArr[$fees->getId()] = $fees->getData();
						}
					}
				}
			}
		}
		
		if(empty($feeCollection->getData()) && empty($newFeesArr))
		{
			return $totalSegments;
		}
		
		if(!empty($newFeesArr))
		{
			$this->setFinalDatas($subtotal, $newFeesArr, false);
		}
		
        $attributes = $totalSegments[$this->code]->getExtensionAttributes();
		if ($attributes === null) {
            $attributes = $this->totalSegmentFactory->create();
        }
		
		if(version_compare($this->dataHelper->getM2Version(), '2.2', '>='))
		{
			$attributes->setMafeeGrandtotalDetails($this->finalData);
		}
        $totalSegments[$this->code]->setExtensionAttributes($attributes);
        return $totalSegments;
    }
	
	public function setFinalDatas($subtotal, $feeCollection, $skip)
	{
		if(!isset($detailsId))
		{
			$detailsId = 1;
		}
		$amount = 0;

		$quote = $this->_checkoutSession->getQuote();
        $method = $quote->getPayment()->getMethod();
		foreach ($feeCollection as $fee) 
		{
			$fees = new \Magento\Framework\DataObject();
			$fees->setData($fee);
			
			if($skip && $fees->getAreaToDisplay() == 'payment_fee_on_product')
			{
				continue;
			}
			
			$percent = null;
            if ($fees->getAreaToDisplay() == 'payment' || $fees->getAreaToDisplay() == 'payment_fee_on_product') {
                $methodArr = explode(',', $fees->getPaymentMethod());
                if (!in_array($method, $methodArr)) {
                    continue;
                }
            }

            if ($fees->getFeeType() == 'percentage') {
				
                //$feeAmount = $this->priceCurrency->round(($subtotal * $fees->getFeeAmount()) / 100);
                $feeAmount = ($subtotal * $fees->getFeeAmount()) / 100;
                $percent = $fees->getFeeAmount() ? $fees->getFeeAmount().'%' : null;
				$label = $fees->getFeeLabel();
            } else {
                $feeAmount = $fees->getFeeAmount();
                $label = $fees->getFeeLabel();
            }
			
			if($this->dataHelper->getFeePerUnit() && $fees->getQty())
			{
				$amount = ($fees->getQty() * $feeAmount);
			} else {
				$amount = ($feeAmount);
			}
			
			if($fees->getItemPrice() > 0)
			{
				$amount = $fees->getItemPrice();
			}
			
			$feeDetails = $this->detailsFactory->create([]);
			$feeDetails->setAmount($amount);
			$feeRates = $this->getRatesData([['percent'=> $percent/* null */,'title'=> $label]]);
			$feeDetails->setRates($feeRates);
			$feeDetails->setGroupId($detailsId);
			$this->finalData[] = $feeDetails;
			
			$detailsId++;
        }
		
	}
}
