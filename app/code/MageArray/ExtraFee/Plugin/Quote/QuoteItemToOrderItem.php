<?php
namespace MageArray\ExtraFee\Plugin\Quote;

use Magento\Quote\Model\Quote\Item\ToOrderItem;
use Magento\Quote\Model\Quote\Item\AbstractItem;

class QuoteItemToOrderItem {
	public function aroundConvert(
		ToOrderItem $subject,
		\Closure $proceed,
		AbstractItem $item,
		$additional = []
	) {
		$orderItem = $proceed($item, $additional);
		if ($additional = $item->getOptionByCode('additional_options')) 
		{
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$helper = $objectManager->get('MageArray\ExtraFee\Helper\Data');
			
			$options = $orderItem->getProductOptions();
			if (version_compare($helper->getM2Version(), '2.2', '>='))
			{
				$options['additional_options'] = json_decode($additional->getValue(),true);
			} else {
				$options['additional_options'] = unserialize($additional->getValue());
			}
			
			$orderItem->setProductOptions($options);
		}
		return $orderItem;
	}
}