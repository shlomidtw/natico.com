<?php
namespace MageArray\ExtraFee\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        /**
         * Create table 'magearray_extrafee'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('magearray_extrafee')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'ExtraFee ID'
        )->addColumn(
            'fee_label',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'ExtraFee Label'
        )->addColumn(
            'fee_amount',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '12,2',
            [],
            'ExtraFee Amount'
        )->addColumn(
            'fee_type',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            200,
            ['nullable' => false],
            'ExtraFee Type'
        )->addColumn(
            'area_to_display',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            200,
            [],
            'Area to display Extra Fee'
        )->addColumn(
            'product_sku',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            ['nullable' => false],
            'Product Sku'
        )->addColumn(
            'categorylist',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            200,
            [],
            'Category List'
        )->addColumn(
            'apply_for_all_product',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'default' => 0],
            'Fee Apply For All Product'
        )->addColumn(
            'payment_method',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            200,
            [],
            'Payment Method'
        )->addColumn(
            'store_ids',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            200,
            [],
            'Store Ids'
        )->addColumn(
            'status',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true],
            'ExtraFee Status'
        )->addColumn(
            'priority',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true],
            'Priority'
        )->setComment(
            'ExtraFee'
        );

        $installer->getConnection()->createTable($table);

        /**
         * Create table 'magearray_sales_order_fee'
         */

        $salesTable = $installer->getConnection()->newTable(
            $installer->getTable('magearray_sales_order_fee')
        )->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Sales Fee ID'
        )->addColumn(
            'order_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true],
            'Order Id'
        )->addColumn(
            'quote_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true],
            'Quote Id'
        )->addColumn(
            'item_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true],
            'Quote Item Id'
        )->addColumn(
            'real_amount',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '12,4',
            [],
            'Real Amount'
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true],
            'Ma Extar Fee Id'
        )->addColumn(
            'fee_label',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'ExtraFee Label'
        )->addColumn(
            'fee_amount',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '12,2',
            [],
            'ExtraFee Amount'
        )->addColumn(
            'fee_type',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            200,
            ['nullable' => false],
            'ExtraFee Type'
        )->addColumn(
            'area_to_display',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            200,
            [],
            'Area to display Extra Fee'
        )->addColumn(
            'product_sku',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            ['nullable' => false],
            'Product Sku'
        )->addColumn(
            'categorylist',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            200,
            [],
            'Category List'
        )->addColumn(
            'apply_for_all_product',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'default' => 0],
            'Fee Apply For All Product'
        )->addColumn(
            'payment_method',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            200,
            [],
            'Payment Method'
        )->addColumn(
            'store_ids',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            200,
            [],
            'Store Ids'
        )->addColumn(
            'status',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true],
            'ExtraFee Status'
        )->addColumn(
            'priority',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true],
            'Priority'
        )->setComment(
            'ExtraFee'
        );
        $installer->getConnection()->createTable($salesTable);

        /**
         * add extra column to appropriate table
         */
        $columns = [
            'ma_extra_fee' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'length' => '12,4',
                'nullable' => false,
                'default' => 0,
                'comment' => 'MageArray Extra fee',
            ],
        ];

        $baseColumns = [
            'base_ma_extra_fee' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'length' => '12,4',
                'nullable' => false,
                'default' => 0,
                'comment' => 'MageArray Base Extra fee',
            ],
        ];

        $quoteItem = $installer->getTable('quote_item');
        $salesTable = $installer->getTable('sales_order');
        $salesAddressTable = $installer->getTable('sales_order_address');
        $invoiceTable = $installer->getTable('sales_invoice');
        $creditMemoTable = $installer->getTable('sales_creditmemo');
        $quoteTable = $installer->getTable('quote');
        $quoteAddressTable = $installer->getTable('quote_address');
        $tableList = [
            $quoteItem,
            $salesTable,
            $salesAddressTable,
            $invoiceTable,
            $creditMemoTable,
            $quoteTable,
            $quoteAddressTable
        ];

        $connection = $installer->getConnection();
        foreach ($columns as $name => $definition) {
            for ($count = 0; $count < count($tableList); $count++) {
                $connection->addColumn($tableList[$count], $name, $definition);
            }
        }

        foreach ($baseColumns as $name => $definition) {
            for ($count = 0; $count < count($tableList); $count++) {
                $connection->addColumn($tableList[$count], $name, $definition);
            }
        }

        $installer->endSetup();
    }
}
