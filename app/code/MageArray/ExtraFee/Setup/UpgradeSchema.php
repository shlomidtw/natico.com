<?php
namespace MageArray\ExtraFee\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Upgrade the Catalog module DB scheme
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
		$installer = $setup;
        $installer->startSetup();
		
        if (version_compare($context->getVersion(), '1.0.2') < 0) 
		{
            $installer->getConnection()->addColumn(
                $installer->getTable('magearray_extrafee'),
                'option_sku',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    'nullable' => true,
                    'comment' => 'Option SKU'
                ]
            ); 
			
			$installer->getConnection()->addColumn(
                $installer->getTable('magearray_sales_order_fee'),
                'option_sku',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    'nullable' => true,
                    'comment' => 'Option SKU'
                ]
            );
        }
        $installer->endSetup();
    }
}
