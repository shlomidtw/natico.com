define([
    'jquery',
    'underscore',
	'Magento_Catalog/js/price-utils'
], function ($, _, priceUtils) {
    'use strict';
	
	$.widget('mage.optionsFee',{
		allOptions: {},
		_create: function() {
			$.extend(this, this.options.optionConfig);
			$.extend(this.allOptions, this.options.optionConfig);
			
			this.loadDataProcess();
		},
		loadDataProcess: function () {
			var MA = this;
			var options = $(".product-custom-option");
			console.log(this.allOptions);
			$.each(options, function(i, option){
				$(option).on('change', function(){
					MA.changeBasePrice(this);
				});	
			});
			
			if(!$('.ma_extra_fees').length)
			{
				$('.product-info-price').after('<div class="ma_extra_fees"><ul class="ma_fee_list"></ul></div>')
			}
		},
		changeBasePrice: function(element){
			var MA = this;
			var option_id = element.name.replace(/[^0-9]/g,'');
			var optionData = this.allOptions[option_id];
			
			if(optionData.sku)
			{
				var fees = optionData.option_fee;
				var name = element.id;
				if(element.value != '' && _.size(optionData.option_fee) > 0)
				{
					MA.addFees(fees, name);
				} else {
					MA.addFees(null, name);
				}
			}
			else if(_.size(optionData) > 0)
			{
				$.each(optionData, function(i, option){
					var fees = option.option_fee;
					var name = element.id + i;
					
					if(element.type == 'select-multiple')
					{
						if($.inArray(i, $(element).val()) !== -1)
						{
							MA.addFees(fees,name);
						} else {
							MA.addFees(null,name);
						}
					}
					
					if(element.type == 'checkbox')
					{
						if($(element).val() == i)
						{
							if($(element).is(':checked'))
							{
								MA.addFees(fees,name);
							} else {
								MA.addFees(null,name);
							}
						}
					}
					
					if(element.type == 'select-one')
					{
						if($(element).val() == i)
						{
							MA.addFees(fees,name);
						} else {
							MA.addFees(null,name);
						}
					}
					
					if(element.type == 'radio')
					{
						name = element.type;
						name += '_';
						name += i;
						
						if($(element).is(':checked') && $(element).val() == i && _.size(fees) > 0)
						{
							MA.addFees(fees,name);
						} else {
							MA.addFees(null,name);
						}
					}
				});
			}
		},
		addFees: function (fees,name){
			var UL = $('.ma_extra_fees ul.ma_fee_list');
			UL.find('[data-id="'+name+'"]').remove();
			if(_.size(fees) > 0)
			{
				UL.append('<li data-id="'+name+'"><span>'+fees.fee_label+':'+fees.amount_title+'</span></li>');
			}
		}
		
	});
	return $.mage.optionsFee;
});