/*global define*/
define(
    [
        'MageArray_ExtraFee/js/view/checkout/summary/ma_summary_fee'
    ],
    function (Component) {
        "use strict";
        return Component.extend({
            defaults: {
                template: 'MageArray_ExtraFee/checkout/cart/totals/ma_cart_fee'
            }
        });
    }
);
