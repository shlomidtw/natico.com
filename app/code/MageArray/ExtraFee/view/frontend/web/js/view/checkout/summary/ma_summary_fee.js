/**
 * Copyright � 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global define*/
define(
    [
        'ko',
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/totals'
    ],
    function (ko, Component, quote, totals) {
        "use strict";
        var isFullTaxSummaryDisplayed = true;
        var isZeroTaxDisplayed = true;
        return Component.extend({
			sititle: '',
            defaults: {
                notCalculatedMessage: 'Not yet calculated',
                template: 'MageArray_ExtraFee/checkout/summary/ma_summary_fee'
            },
            totals: quote.getTotals(),
            isFullTaxSummaryDisplayed: isFullTaxSummaryDisplayed,
            ifShowValue: function() {
				if(totals.getSegment('ma_extra_fee') == null)
				{
					return false;
				}
				this.sititle = totals.getSegment('ma_extra_fee').title;
                if (this.isFullMode() && this.getPureValue() == 0) {
                    return isZeroTaxDisplayed;
                }
                return true;
            },
            ifShowDetails: function() {
                if (!this.isFullMode()) {
                    return false;
                }
                return this.getPureValue() > 0 && isFullTaxSummaryDisplayed;
            },
            getPureValue: function() {
                var amount = 0;
                if (this.totals()) {
					
                    var feeTotal = totals.getSegment('ma_extra_fee');
                    if (feeTotal) {
                        amount = feeTotal.value;
                    }
                }
				
                return amount;
            },
            isCalculated: function() {
                return this.totals() && this.isFullMode() && null != totals.getSegment('ma_extra_fee');
            },
            getValue: function() {
                if (!this.isCalculated()) {
                    return this.notCalculatedMessage;
                }
                var amount = totals.getSegment('ma_extra_fee').value;
                return this.getFormattedPrice(amount);
            },
            formatPrice: function(amount) {
                return this.getFormattedPrice(amount);
            },
            getDetails: function() {
                var feeSegment = totals.getSegment('ma_extra_fee');
                if (feeSegment && feeSegment.extension_attributes) {
                    return feeSegment.extension_attributes.mafee_grandtotal_details;
                }
                return [];
            },
			getTitle: function () {
				/* var sititle = "Additional Fees";
				if (this.totals() && totals.getSegment('ma_extra_fee')) {
					sititle = totals.getSegment('ma_extra_fee').title;
				} */
				return this.sititle;
			}
        });
    }
);
