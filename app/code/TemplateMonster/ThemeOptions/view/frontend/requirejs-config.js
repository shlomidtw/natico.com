var config = {
    map: {
        '*': {}
    },
    paths: {
        "productListingGallery":    "TemplateMonster_ThemeOptions/js/product-listing-gallery",
        "switchImage":              "TemplateMonster_ThemeOptions/js/switch-image",
        "stickUp":                  "TemplateMonster_ThemeOptions/js/stickUp",
        "themeOptions":             "TemplateMonster_ThemeOptions/js/theme-options",
        "tiltJs":                   "TemplateMonster_ThemeOptions/js/tilt.jquery.min"
    },
    shim: {
        "stickUp":  ["jquery"],
        "tiltJs":   ["jquery"]
    },
};